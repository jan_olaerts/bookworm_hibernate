-- dropping tables
DROP TABLE IF EXISTS Books;
DROP TABLE IF EXISTS Archived_Books;

-- creating tables
CREATE TABLE Books(
    id           INT         NOT NULL IDENTITY,
    title        VARCHAR(45) NOT NULL,
    author       VARCHAR(45) NOT NULL,
    isbn         VARCHAR(45) NOT NULL,
    revisionNr   INT         NOT NULL,
    genre        VARCHAR(45) NOT NULL,
    fiction      TINYINT     NOT NULL
);

CREATE TABLE Archived_Books(
    id           INT         NOT NULL IDENTITY,
    title        VARCHAR(45) NOT NULL,
    author       VARCHAR(45) NOT NULL,
    isbn         VARCHAR(45) NOT NULL,
    revisionNr   INT         NOT NULL,
    genre        VARCHAR(45) NOT NULL,
    fiction      TINYINT     NOT NULL,
    archivedDate DATE        NOT NULL
);