-- inserting data into tables
INSERT INTO Books (id, title, author, isbn, revisionNr, genre, fiction) VALUES (1, 'Moby Dick', 'Herman Melville', '978-0-14-086172-3', 1, 'SCI_FI', 1);
INSERT INTO Books (id, title, author, isbn, revisionNr, genre, fiction) VALUES (2, 'Gulliver''s Travels', 'Jonathan Swift', '978-0-14-086272-0', 1, 'FANTASY', 1);
INSERT INTO Books (id, title, author, isbn, revisionNr, genre, fiction) VALUES (3, 'Robinson Crusoe', 'Daniel Defoe', '978-0-00-105242-0', 1, 'CONTEMPORARY', 0);
INSERT INTO Books (id, title, author, isbn, revisionNr, genre, fiction) VALUES (4, 'The Little Prince', 'Antoine De Saint Exupery', '978-0-15-601207-2', 1, 'ADVENTURE', 1);
INSERT INTO Books (id, title, author, isbn, revisionNr, genre, fiction) VALUES (5, 'The Hobbit', 'Tolkien', '978-0-00-752550-8', 1, 'ADVENTURE', 1);
INSERT INTO Books (id, title, author, isbn, revisionNr, genre, fiction) VALUES (6, 'The Kite Runner', 'Khaled Hosseini', '978-0-74-758894-8', 1, 'DRAMA', 0);
INSERT INTO Books (id, title, author, isbn, revisionNr, genre, fiction) VALUES (7, 'The House Of The Spirits', 'Isabel Allende', '978-0-60-622871-8', 1, 'DRAMA', 0);
INSERT INTO Books (id, title, author, isbn, revisionNr, genre, fiction) VALUES (8, 'A Thousand Splendid Suns', 'Khaled Hosseini', '978-0-74-758279-3', 1, 'DRAMA', 0);
INSERT INTO Books (id, title, author, isbn, revisionNr, genre, fiction) VALUES (9, 'The Da Vinci Code', 'Dan Brown', '978-0-37-543230-9', 1, 'MYSTERY', 1);
INSERT INTO Books (id, title, author, isbn, revisionNr, genre, fiction) VALUES (10, 'Angels And Demons', 'Dan Brown', '978-0-55-215970-8', 1, 'MYSTERY', 1);

INSERT INTO Archived_Books (id, title, author, isbn, revisionNr, genre, fiction, archivedDate) VALUES (1, 'Book Of Jan', 'Jan', '978-0-14-311239-3', 1, 'MYSTERY', 0, '2020-04-06');
INSERT INTO Archived_Books (id, title, author, isbn, revisionNr, genre, fiction, archivedDate) VALUES (2, 'Digital Fortress', 'Dan Brown', '978-0-31-218087-4', 1, 'MYSTERY', 1, '2018-12-06');
INSERT INTO Archived_Books (id, title, author, isbn, revisionNr, genre, fiction, archivedDate) VALUES (3, 'The Lost Symbol', 'Dan Brown', '978-0-30-774190-5', 1, 'MYSTERY', 1, '2019-10-08');
INSERT INTO Archived_Books (id, title, author, isbn, revisionNr, genre, fiction, archivedDate) VALUES (4, 'Matilda', 'Roald Dahl', '978-0-14-134679-3', 1, 'FANTASY', 1, '2020-08-10');
INSERT INTO Archived_Books (id, title, author, isbn, revisionNr, genre, fiction, archivedDate) VALUES (5, 'One Hundred Years Of Solitude', 'Gabriel Garcia Marquez', '978-0-06-088328-7', 1, 'MYSTERY', 0, '2020-01-02');
INSERT INTO Archived_Books (id, title, author, isbn, revisionNr, genre, fiction, archivedDate) VALUES (6, 'The Shadow Of The Wind', 'Carlos Ruiz Zafon', '978-0-14-312639-3', 1, 'MYSTERY', 0, '2019-11-18');
INSERT INTO Archived_Books (id, title, author, isbn, revisionNr, genre, fiction, archivedDate) VALUES (7, 'La Ciudad Y Los Perros', 'Mario Vargas Llosa', '978-8-42-046706-1', 1, 'MYSTERY', 0, '2018-07-07');
INSERT INTO Archived_Books (id, title, author, isbn, revisionNr, genre, fiction, archivedDate) VALUES (8, 'Death In The Andes', 'Mario Vargas Llosa', '978-3-51-839728-2', 1, 'MYSTERY', 0, '2019-07-07');
INSERT INTO Archived_Books (id, title, author, isbn, revisionNr, genre, fiction, archivedDate) VALUES (9, '1984', 'George Orwell', '978-0-14-103614-4', 1, 'MYSTERY', 1, '2020-04-06');
INSERT INTO Archived_Books (id, title, author, isbn, revisionNr, genre, fiction, archivedDate) VALUES (10, 'Het Parfum', 'Patrick Suskind', '978-9-04-460676-8', 1, 'MYSTERY', 1, '2020-03-22');