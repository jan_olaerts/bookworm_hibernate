package be.janolaerts.bookworm.entities;

import be.janolaerts.bookworm.exception.BookException;
import be.janolaerts.bookworm.genres.Genres;
import be.janolaerts.bookworm.testTools.SystemInOutTester;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BookTest extends SystemInOutTester {

    Book book;

    @BeforeEach
    public void makeBook() {
        book = new Book();
    }

    @Test
    void setTitle() {
        assertThrows(BookException.class, () -> book.setTitle(null));

        book.setTitle("This is a title");
        assertEquals("This is a title", book.getTitle());
    }

    @Test
    void setAuthor() {
        assertThrows(BookException.class, () -> book.setAuthor(null));

        book.setAuthor("This is an author");
        assertEquals("This is an author", book.getAuthor());
    }

    @Test
    void setIsbn() {
        assertThrows(BookException.class, () -> book.setIsbn("abc"));
        assertThrows(BookException.class, () -> book.setIsbn("012345678"));
        assertThrows(BookException.class, () -> book.setIsbn("01234567856"));

        book.setIsbn("978-3-16-148410-0");
        assertEquals("978-3-16-148410-0", book.getIsbn());

        book.setIsbn("9783161484100");
        assertEquals("978-3-16-148410-0", book.getIsbn());
    }

    @Test
    void setRevisionNr() {
        assertThrows(BookException.class, () -> book.setRevisionNr(-1));

        book.setRevisionNr(1524);
        assertEquals(1524, book.getRevisionNr());
    }

    @Test
    void setGenre() {
        assertThrows(BookException.class, () -> book.setGenre(null));

        book.setGenre(Genres.CONTEMPORARY);
        assertEquals(Genres.CONTEMPORARY, book.getGenre());
    }

    @Test
    void setFiction() {
        book.setFiction(true);
        assertTrue(book.isFiction());

        book.setFiction(false);
        assertFalse(book.isFiction());
    }
}