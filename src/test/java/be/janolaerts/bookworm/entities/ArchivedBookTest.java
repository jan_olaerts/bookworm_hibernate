package be.janolaerts.bookworm.entities;

import be.janolaerts.bookworm.exception.BookException;
import be.janolaerts.bookworm.genres.Genres;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class ArchivedBookTest {

    ArchivedBook archivedBook;

    @BeforeEach
    public void makeArchivedBook() {
        archivedBook = new ArchivedBook();
    }

    @Test
    void setTitle() {
        assertThrows(BookException.class, () -> archivedBook.setTitle(null));

        archivedBook.setTitle("This is a title");
        assertEquals("This is a title", archivedBook.getTitle());
    }

    @Test
    void setAuthor() {
        assertThrows(BookException.class, () -> archivedBook.setAuthor(null));

        archivedBook.setAuthor("This is an author");
        assertEquals("This is an author", archivedBook.getAuthor());
    }

    @Test
    void setIsbn() {
        assertThrows(BookException.class, () -> archivedBook.setIsbn("abc"));
        assertThrows(BookException.class, () -> archivedBook.setIsbn("012345678"));
        assertThrows(BookException.class, () -> archivedBook.setIsbn("01234567856"));

        archivedBook.setIsbn("978-3-16-148410-0");
        assertEquals("978-3-16-148410-0", archivedBook.getIsbn());

        archivedBook.setIsbn("9783161484100");
        assertEquals("978-3-16-148410-0", archivedBook.getIsbn());
    }

    @Test
    void setRevisionNr() {
        assertThrows(BookException.class, () -> archivedBook.setRevisionNr(-1));

        archivedBook.setRevisionNr(1524);
        assertEquals(1524, archivedBook.getRevisionNr());
    }

    @Test
    void setGenre() {
        assertThrows(BookException.class, () -> archivedBook.setGenre(null));

        archivedBook.setGenre(Genres.CONTEMPORARY);
        assertEquals(Genres.CONTEMPORARY, archivedBook.getGenre());
    }

    @Test
    void setFiction() {
        archivedBook.setFiction(true);
        assertTrue(archivedBook.isFiction());

        archivedBook.setFiction(false);
        assertFalse(archivedBook.isFiction());
    }

    @Test
    void setArchivedDate() {
        assertThrows(BookException.class, () -> archivedBook.setArchivedDate(null));
        assertThrows(BookException.class, () -> archivedBook.setArchivedDate(LocalDate.of(2020, 11, 25)));

        archivedBook.setArchivedDate(LocalDate.now());
        assertEquals(LocalDate.now(), archivedBook.getArchivedDate());
    }
}