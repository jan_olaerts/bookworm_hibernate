package be.janolaerts.bookworm.daos;

import be.janolaerts.bookworm.entities.ArchivedBook;
import be.janolaerts.bookworm.entities.Book;
import be.janolaerts.bookworm.exception.BookException;
import be.janolaerts.bookworm.genres.Genres;
import be.janolaerts.bookworm.tools.HibernateTool;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.hibernate.Session;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ArchivedBookDAOTest {

    private static ArchivedBookDAO archivedBookDAO;

    @BeforeEach
    void init() {
        EntityManager em = HibernateTool.getEntityManager();
        archivedBookDAO = new ArchivedBookDAO();
        Session session = (Session) em.getDelegate();

        session.doWork(connection -> {

            try {
                ScriptRunner scriptRunner = new ScriptRunner( connection );

                scriptRunner.setLogWriter(null);

                BufferedReader bufferedReader = new BufferedReader(new FileReader("src/test/resources/createTables.sql"));
                scriptRunner.runScript(bufferedReader);

                bufferedReader = new BufferedReader(new FileReader("src/test/resources/insertTables.sql"));
                scriptRunner.runScript(bufferedReader);

            } catch (FileNotFoundException fnfe) {
                System.out.println("File not found");
            }
        });
    }

    @Test
    void saveArchivedBook() {
        assertThrows(BookException.class, () -> archivedBookDAO.saveArchivedBook(null));

        ArchivedBook archivedBook = new ArchivedBook(new Book(
                "Test", "Test", "9785412563254", 52, Genres.SCI_FI, true));

        archivedBookDAO.saveArchivedBook(archivedBook);
        assertEquals(11, archivedBookDAO.getArchivedBooks("getAllArchivedBooks").size());
    }

    @Test
    void getArchivedBooks() {
        assertThrows(BookException.class, () -> archivedBookDAO.getArchivedBooks(null));

        List<ArchivedBook> archivedBooks;

        archivedBooks = archivedBookDAO.getArchivedBooks("getArchivedBooksOrderedByTitle");
        assertEquals("1984", archivedBooks.get(0).getTitle());
        assertEquals("The Shadow Of The Wind", archivedBooks.get(9).getTitle());

        archivedBooks = archivedBookDAO.getArchivedBooks("getArchivedBooksOrderedByArchivedDate");
        assertEquals("La Ciudad Y Los Perros", archivedBooks.get(0).getTitle());
        assertEquals("Matilda", archivedBooks.get(9).getTitle());
    }
}