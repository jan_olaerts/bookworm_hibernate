package be.janolaerts.bookworm.daos;

import be.janolaerts.bookworm.entities.Book;
import be.janolaerts.bookworm.exception.BookException;
import be.janolaerts.bookworm.genres.Genres;
import be.janolaerts.bookworm.tools.HibernateTool;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.hibernate.Session;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BookDaoTest {

    private static EntityManager em;
    private static BookDao bookDao;

    @BeforeEach
    void init() {
        em = HibernateTool.getEntityManager();
        bookDao = new BookDao();
        Session session = (Session) em.getDelegate();

        session.doWork(connection -> {

            try {
                ScriptRunner scriptRunner = new ScriptRunner(connection);

                scriptRunner.setLogWriter(null);

                BufferedReader bufferedReader = new BufferedReader(new FileReader("src/test/resources/createTables.sql"));
                scriptRunner.runScript(bufferedReader);

                bufferedReader = new BufferedReader(new FileReader("src/test/resources/insertTables.sql"));
                scriptRunner.runScript(bufferedReader);

            } catch (FileNotFoundException fnfe) {
                System.out.println("File not found");
            }
        });
    }

    @Test
    void saveOrDeleteBook() {
        assertThrows(BookException.class, () -> bookDao.saveOrDeleteBook(null, "save"));
        assertThrows(BookException.class, () -> bookDao.saveOrDeleteBook(new Book(), "Save"));

        Book book = new Book("Test", "Test", "978-5-41-256325-4", 1, Genres.CONTEMPORARY, true);
        bookDao.saveOrDeleteBook(book, "save");
        assertEquals(11, bookDao.getBooks("getAllBooks").size());
    }

    @Test
    void getBook() {
        assertThrows(BookException.class, () -> bookDao.getBooks(null));

        Book book = bookDao.getBook("getBookById", 1L);
        assertEquals(1, book.getId());
        assertEquals("Moby Dick", book.getTitle());
        assertEquals("Herman Melville", book.getAuthor());
        assertEquals("978-0-14-086172-3", book.getIsbn());
        assertEquals(1, book.getRevisionNr());
        assertEquals(Genres.SCI_FI, book.getGenre());
        assertTrue(book.isFiction());
    }

    @Test
    void getRandomBook() {

        boolean notEqual = false;

        List<Book> books = new ArrayList<>();
        for(int i = 0; i < 10; i++) {
            books.add(bookDao.getRandomBook());
        }

        Book book = books.get(0);
        for (Book value : books) {
            if (book != value) {
                notEqual = true;
                break;
            }
        }

        assertTrue(notEqual);
    }

    @Test
    void getBooks() {
        assertThrows(BookException.class, () -> bookDao.getBooks(null));

        List<Book> books;

        books = bookDao.getBooks("getBooksByGenre", Genres.ADVENTURE);
        assertEquals(2, books.size());

        books = bookDao.getBooks("getBooksByAuthor", "%Da%");
        assertEquals(3, books.size());

        books = bookDao.getBooks("getAllBooksOrderedByTitleAuthor");
        assertEquals(10, books.size());
    }

    @Test
    void setParams() {
        assertThrows(BookException.class, () -> BookDao.setParams(null));

        TypedQuery<Book> query;
        Book book;

        query = em.createNamedQuery("getBookById", Book.class);
        TypedQuery<Book> finalQuery = query;
        assertThrows(NullPointerException.class, () -> BookDao.setParams(finalQuery, null));

        query = em.createNamedQuery("getBookById", Book.class);
        BookDao.setParams(query, 3L);
        book = query.getSingleResult();
        assertEquals("Robinson Crusoe", book.getTitle());

        query = em.createNamedQuery("getBookByTitle", Book.class);
        BookDao.setParams(query, "The Kite Runner");
        book = query.getSingleResult();
        assertEquals("Khaled Hosseini", book.getAuthor());
    }
}