package be.janolaerts.bookworm.menus;

import be.janolaerts.bookworm.entities.ArchivedBook;
import be.janolaerts.bookworm.testTools.SystemInOutTester;
import be.janolaerts.bookworm.tools.HibernateTool;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.hibernate.Session;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

import static org.junit.jupiter.api.Assertions.*;

class MenuActionsTest extends SystemInOutTester {

    private static EntityManager em = null;

    @BeforeAll
    static void init() {
        em = HibernateTool.getEntityManager();
        Session session = (Session) em.getDelegate();

        session.doWork(connection -> {

            try {
                ScriptRunner scriptRunner = new ScriptRunner(connection);

                scriptRunner.setLogWriter(null);

                BufferedReader bufferedReader = new BufferedReader(new FileReader("src/test/resources/createTables.sql"));
                scriptRunner.runScript(bufferedReader);

                bufferedReader = new BufferedReader(new FileReader("src/test/resources/insertTables.sql"));
                scriptRunner.runScript(bufferedReader);

            } catch (FileNotFoundException fnfe) {
                System.out.println("File not found");
            }
        });
    }

    @Test
    void determineSubMenu() {
        assertThrows(IllegalArgumentException.class, () -> MenuActions.determineSubMenu(-1));
        assertThrows(IllegalArgumentException.class, () -> MenuActions.determineSubMenu(5));

        MenuActions.determineSubMenu(0);
        assertEquals("", getOutput());

        resetStreams();
        addInputLine("0");
        MenuActions.determineSubMenu(1);
        assertEquals("\r\n" +
                "\u001B[34mBooks Menu\u001B[0m\r\n" +
                "\u001B[34m-------------------------------------------------------------------------------------\u001B[0m\r\n" +
                "\u001B[34m 1 | Show All Books Ordered By Id                   7 | Get Book By Isbn\u001B[0m\r\n" +
                "\u001B[34m 2 | Show All Books Ordered By Title                8 | Get Book By Title\u001B[0m\r\n" +
                "\u001B[34m 3 | Show All Books Ordered By Author               9 | Get Books By Author\u001B[0m\r\n" +
                "\u001B[34m 4 | Show All Books Ordered By Isbn                10 | Get Books By Genre\u001B[0m\r\n" +
                "\u001B[34m 5 | Show All Books Ordered By Genre               11 | Get A Random Book\u001B[0m\r\n" +
                "\u001B[34m 6 | Show All Books Ordered By Title and Author    12 | Get First Book\u001B[0m\r\n" +
                "\u001B[34m13 | Get Last Book                                 \u001B[33m 0 | Back To The Main Menu \u001B[0m\u001B[0m\r\n" +
                "\u001B[34m-------------------------------------------------------------------------------------\u001B[0m\r\n" +
                "\u001B[34mGive a number between 0 and 13: \u001B[0m", getOutput());

        resetStreams();
        addInputLine("0");
        MenuActions.determineSubMenu(2);
        assertEquals("\r\n" +
                "\u001B[34mArchived Books Menu\u001B[0m\r\n" +
                "\u001B[34m--------------------------------------------------------\u001B[0m\r\n" +
                "\u001B[34m1 | Show All Archived Books Ordered By Title\u001B[0m\r\n" +
                "\u001B[34m2 | Show All Archived Books Ordered By Date Archived\u001B[0m\r\n" +
                "\u001B[33m0 | Head menu\u001B[0m\r\n" +
                "\u001B[34m--------------------------------------------------------\u001B[0m\r\n" +
                "\u001B[34mGive a number between 0 and 2: \u001B[0m", getOutput());

        resetStreams();
        setInput("test");
        addInputLine("test");
        addInputLine("9784563254789");
        addInputLine("1");
        addInputLine("1");
        addInputLine("y");
        addInputLine("\n");
        MenuActions.determineSubMenu(3);
        assertEquals("\r\n" +
                "\u001B[34mAdd a Book\u001B[0m\r\n" +
                "\u001B[34m--------------------------------------------\u001B[0m\r\n" +
                "\u001B[34mTitle: \u001B[0m\u001B[34mAuthor: \u001B[0m\u001B[34mIsbn: \u001B[0m\u001B[34mRevision Nr: \u001B[0m\r\n" +
                "\u001B[34m1| ADVENTURE\u001B[0m\r\n" +
                "\u001B[34m2| CONTEMPORARY\u001B[0m\r\n" +
                "\u001B[34m3| DRAMA\u001B[0m\r\n" +
                "\u001B[34m4| FANTASY\u001B[0m\r\n" +
                "\u001B[34m5| MYSTERY\u001B[0m\r\n" +
                "\u001B[34m6| ROMANCE\u001B[0m\r\n" +
                "\u001B[34m7| SCI_FI\u001B[0m\r\n" +
                "\u001B[34m8| THRILLER\u001B[0m\r\n" +
                "\u001B[34m9| WESTERN\u001B[0m\r\n" +
                "\u001B[34mGenre: \u001B[0m\r\n" +
                "\u001B[34mFiction (y/n): \u001B[0m\r\n" +
                "\u001B[33mBook successfully saved\u001B[0m\r\n" +
                "\u001B[34mPress enter to continue.\u001B[0m", getOutput());

        resetStreams();
        setInput("dfhgdfgh");
        addInputLine("y");
        addInputLine("\n");
        MenuActions.determineSubMenu(4);
        assertTrue(getOutput().contains("No book found"));
    }

    @Test
    void archiveBook() {

        setInput("house");
        addInputLine("y");
        addInputLine("\n");
        MenuActions.archiveBook();
        assertTrue(getOutput().contains("The House Of The Spirits"));
        assertTrue(getOutput().contains("978-0-60-622871-8"));
        assertTrue(getOutput().contains("Do you want to archive this book? (y/n):"));
        assertTrue(getOutput().contains("Book successfully archived"));
        assertTrue(getOutput().contains("Press enter to continue."));

        TypedQuery<ArchivedBook> query =
                em.createQuery("select ab from ArchivedBook ab where ab.isbn = '978-0-60-622871-8'", ArchivedBook.class);

        ArchivedBook archivedBook = query.getSingleResult();
        assertEquals("The House Of The Spirits", archivedBook.getTitle());
    }
}