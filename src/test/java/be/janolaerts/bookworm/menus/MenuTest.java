package be.janolaerts.bookworm.menus;

import be.janolaerts.bookworm.testTools.SystemInOutTester;
import be.janolaerts.bookworm.tools.UserInputTool;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MenuTest extends SystemInOutTester {

    public MenuTest(){
        super();
        UserInputTool.getNewScanner();
    }

    @Test
    void showHeadMenu() {

        setInput("5");
        setInput("2");
        addInputLine("0");
        Menu.showHeadMenu();
        assertEquals("\u001B[34mWelcome To The Bookworm App!\u001B[0m\r\n" +
                "\u001B[34m--------------------------------------\u001B[0m\r\n" +
                "\u001B[34m1| Show Books Menu\u001B[0m\r\n" +
                "\u001B[34m2| Show Archived Books Menu\u001B[0m\r\n" +
                "\u001B[34m3| Add A Book\u001B[0m\r\n" +
                "\u001B[34m4| Archive Book\u001B[0m\r\n" +
                "\u001B[34m\u001B[33m0| Exit\u001B[0m\u001B[0m\r\n" +
                "\u001B[34m--------------------------------------\u001B[0m\r\n" +
                "\u001B[34mGive a number between 0 and 4: \u001B[0m\r\n" +
                "\u001B[34mArchived Books Menu\u001B[0m\r\n" +
                "\u001B[34m--------------------------------------------------------\u001B[0m\r\n" +
                "\u001B[34m1 | Show All Archived Books Ordered By Title\u001B[0m\r\n" +
                "\u001B[34m2 | Show All Archived Books Ordered By Date Archived\u001B[0m\r\n" +
                "\u001B[33m0 | Head menu\u001B[0m\r\n" +
                "\u001B[34m--------------------------------------------------------\u001B[0m\r\n" +
                "\u001B[34mGive a number between 0 and 2: \u001B[0m", getOutput());

        resetStreams();
        setInput("5");
        addInputLine("0");
        Menu.showHeadMenu();
        assertEquals("Input must be a number between 0 and 4\n", getError());

        resetStreams();
        setInput("0");
        assertEquals(0, Menu.showHeadMenu());
    }

    @Test
    void showBooksMenu() {

        setInput("14");
        addInputLine("0");
        Menu.showBooksMenu();
        assertEquals("Input must be a number between 0 and 13\n", getError());

        resetStreams();
        setInput("-1");
        addInputLine("0");
        Menu.showBooksMenu();
        assertEquals("Input must be a number between 0 and 13\n", getError());

        resetStreams();
        setInput("0");
        Menu.showBooksMenu();
        assertEquals("\r\n" +
                "\u001B[34mBooks Menu\u001B[0m\r\n" +
                "\u001B[34m-------------------------------------------------------------------------------------\u001B[0m\r\n" +
                "\u001B[34m 1 | Show All Books Ordered By Id                   7 | Get Book By Isbn\u001B[0m\r\n" +
                "\u001B[34m 2 | Show All Books Ordered By Title                8 | Get Book By Title\u001B[0m\r\n" +
                "\u001B[34m 3 | Show All Books Ordered By Author               9 | Get Books By Author\u001B[0m\r\n" +
                "\u001B[34m 4 | Show All Books Ordered By Isbn                10 | Get Books By Genre\u001B[0m\r\n" +
                "\u001B[34m 5 | Show All Books Ordered By Genre               11 | Get A Random Book\u001B[0m\r\n" +
                "\u001B[34m 6 | Show All Books Ordered By Title and Author    12 | Get First Book\u001B[0m\r\n" +
                "\u001B[34m13 | Get Last Book                                 \u001B[33m 0 | Back To The Main Menu \u001B[0m\u001B[0m\r\n" +
                "\u001B[34m-------------------------------------------------------------------------------------\u001B[0m\r\n" +
                "\u001B[34mGive a number between 0 and 13: \u001B[0m", getOutput());
    }

    @Test
    void showArchivedMenu() {

        setInput("3");
        addInputLine("0");
        Menu.showArchivedMenu();
        assertEquals("Input must be a number between 0 and 2\n", getError());

        resetStreams();
        setInput("-1");
        addInputLine("0");
        Menu.showArchivedMenu();
        assertEquals("Input must be a number between 0 and 2\n", getError());

        resetStreams();
        setInput("0");
        Menu.showArchivedMenu();
        assertEquals("\r\n" +
                "\u001B[34mArchived Books Menu\u001B[0m\r\n" +
                "\u001B[34m--------------------------------------------------------\u001B[0m\r\n" +
                "\u001B[34m1 | Show All Archived Books Ordered By Title\u001B[0m\r\n" +
                "\u001B[34m2 | Show All Archived Books Ordered By Date Archived\u001B[0m\r\n" +
                "\u001B[33m0 | Head menu\u001B[0m\r\n" +
                "\u001B[34m--------------------------------------------------------\u001B[0m\r\n" +
                "\u001B[34mGive a number between 0 and 2: \u001B[0m", getOutput());
    }

    @Test
    void addBookMenu() {

        setInput("test");
        addInputLine("test");
        addInputLine("9784563254784");
        addInputLine("1");
        addInputLine("1");
        addInputLine("y");
        addInputLine("");
        //printStatus();
        try {
            Menu.addBookMenu();
        }catch (Exception e){
            printStatus();
            e.printStackTrace();
        }
        assertEquals("\r\n" +
                "\u001B[34mAdd a Book\u001B[0m\r\n" +
                "\u001B[34m--------------------------------------------\u001B[0m\r\n" +
                "\u001B[34mTitle: \u001B[0m\u001B[34mAuthor: \u001B[0m\u001B[34mIsbn: \u001B[0m\u001B[34mRevision Nr: \u001B[0m\r\n" +
                "\u001B[34m1| ADVENTURE\u001B[0m\r\n" +
                "\u001B[34m2| CONTEMPORARY\u001B[0m\r\n" +
                "\u001B[34m3| DRAMA\u001B[0m\r\n" +
                "\u001B[34m4| FANTASY\u001B[0m\r\n" +
                "\u001B[34m5| MYSTERY\u001B[0m\r\n" +
                "\u001B[34m6| ROMANCE\u001B[0m\r\n" +
                "\u001B[34m7| SCI_FI\u001B[0m\r\n" +
                "\u001B[34m8| THRILLER\u001B[0m\r\n" +
                "\u001B[34m9| WESTERN\u001B[0m\r\n" +
                "\u001B[34mGenre: \u001B[0m\r\n" +
                "\u001B[34mFiction (y/n): \u001B[0m\r\n" +
                "\u001B[33mBook successfully saved\u001B[0m\r\n" +
                "\u001B[34mPress enter to continue.\u001B[0m", getOutput());
    }
}