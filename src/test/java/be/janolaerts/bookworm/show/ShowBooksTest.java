package be.janolaerts.bookworm.show;

import be.janolaerts.bookworm.daos.ArchivedBookDAO;
import be.janolaerts.bookworm.daos.BookDao;
import be.janolaerts.bookworm.entities.ArchivedBook;
import be.janolaerts.bookworm.entities.Book;
import be.janolaerts.bookworm.exception.BookException;
import be.janolaerts.bookworm.genres.Genres;
import be.janolaerts.bookworm.testTools.SystemInOutTester;
import be.janolaerts.bookworm.tools.HibernateTool;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.hibernate.Session;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ShowBooksTest extends SystemInOutTester {

    private static EntityManager em;
    private static BookDao bookDao;
    private static final ArchivedBookDAO archivedBookDao = new ArchivedBookDAO();

    @BeforeEach
    void init() {
        em = HibernateTool.getEntityManager();
        bookDao = new BookDao();
        Session session = (Session) em.getDelegate();

        session.doWork(connection -> {

            try {
                ScriptRunner scriptRunner = new ScriptRunner(connection);

                scriptRunner.setLogWriter(null);

                BufferedReader bufferedReader = new BufferedReader(new FileReader("src/test/resources/createTables.sql"));
                scriptRunner.runScript(bufferedReader);

                bufferedReader = new BufferedReader(new FileReader("src/test/resources/insertTables.sql"));
                scriptRunner.runScript(bufferedReader);

            } catch (FileNotFoundException fnfe) {
                System.out.println("File not found");
            }
        });
    }

    @Test
    void doShowBooks() {
        assertThrows(IllegalArgumentException.class, () -> ShowBooks.doShowBooks(-1));
        assertThrows(IllegalArgumentException.class, () -> ShowBooks.doShowBooks(14));

        addInputLine("\n");
        assertEquals(5, ShowBooks.doShowBooks(5));

        resetStreams();
        setInput("\n");
        ShowBooks.doShowBooks(1);
        assertEquals("\u001B[34m\n" +
                "------------------------------------------------------------------------------------------------------------------------------------\u001B[0m\r\n" +
                "\u001B[34mTitle                         | Author                        | ISBN               | Revision Nr       | Genre             | Fiction             \u001B[0m\r\n" +
                "\u001B[34m------------------------------------------------------------------------------------------------------------------------------------\u001B[0m\r\n" +
                "\u001B[34mMoby Dick                     | Herman Melville               | 978-0-14-086172-3  | 1                 | SCI_FI            | true                \u001B[0m\r\n" +
                "\u001B[34mGulliver's Travels            | Jonathan Swift                | 978-0-14-086272-0  | 1                 | FANTASY           | true                \u001B[0m\r\n" +
                "\u001B[34mRobinson Crusoe               | Daniel Defoe                  | 978-0-00-105242-0  | 1                 | CONTEMPORARY      | false               \u001B[0m\r\n" +
                "\u001B[34mThe Little Prince             | Antoine De Saint Exupery      | 978-0-15-601207-2  | 1                 | ADVENTURE         | true                \u001B[0m\r\n" +
                "\u001B[34mThe Hobbit                    | Tolkien                       | 978-0-00-752550-8  | 1                 | ADVENTURE         | true                \u001B[0m\r\n" +
                "\u001B[34mThe Kite Runner               | Khaled Hosseini               | 978-0-74-758894-8  | 1                 | DRAMA             | false               \u001B[0m\r\n" +
                "\u001B[34mThe House Of The Spirits      | Isabel Allende                | 978-0-60-622871-8  | 1                 | DRAMA             | false               \u001B[0m\r\n" +
                "\u001B[34mA Thousand Splendid Suns      | Khaled Hosseini               | 978-0-74-758279-3  | 1                 | DRAMA             | false               \u001B[0m\r\n" +
                "\u001B[34mThe Da Vinci Code             | Dan Brown                     | 978-0-37-543230-9  | 1                 | MYSTERY           | true                \u001B[0m\r\n" +
                "\u001B[34mAngels And Demons             | Dan Brown                     | 978-0-55-215970-8  | 1                 | MYSTERY           | true                \u001B[0m\r\n" +
                "\u001B[34m------------------------------------------------------------------------------------------------------------------------------------\u001B[0m\r\n" +
                "\r\n" +
                "\u001B[34mPress enter to continue.\u001B[0m", getOutput());

        resetStreams();
        setInput("\n");
        ShowBooks.doShowBooks(12);
        assertEquals("\u001B[34m\n" +
                "------------------------------------------------------------------------------------------------------------------------------------\u001B[0m\r\n" +
                "\u001B[34mTitle                         | Author                        | ISBN               | Revision Nr       | Genre             | Fiction             \u001B[0m\r\n" +
                "\u001B[34m------------------------------------------------------------------------------------------------------------------------------------\u001B[0m\r\n" +
                "\u001B[34mMoby Dick                     | Herman Melville               | 978-0-14-086172-3  | 1                 | SCI_FI            | true                \u001B[0m\r\n" +
                "\u001B[34m------------------------------------------------------------------------------------------------------------------------------------\u001B[0m\r\n" +
                "\r\n" +
                "\u001B[34mPress enter to continue.\u001B[0m", getOutput());
    }

    @Test
    void doShowArchivedBooks() {
        assertThrows(BookException.class, () -> ShowBooks.doShowArchivedBooks(-1));
        assertThrows(BookException.class, () -> ShowBooks.doShowArchivedBooks(3));

        assertEquals(0, ShowBooks.doShowArchivedBooks(0));

        setInput("\n");
        ShowBooks.doShowArchivedBooks(1);
        assertEquals("\u001B[34m\n" +
                "---------------------------------------------------------------------------------------------------------------------------------------------------------\u001B[0m\r\n" +
                "\u001B[34mTitle                         | Author                        | ISBN               | Revision Nr       | Genre             | Fiction    | Archived Date       \u001B[0m\r\n" +
                "\u001B[34m---------------------------------------------------------------------------------------------------------------------------------------------------------\u001B[0m\r\n" +
                "\u001B[34m1984                          | George Orwell                 | 978-0-14-103614-4  | 1                 | MYSTERY           | true       | 2020-04-06          \u001B[0m\r\n" +
                "\u001B[34mBook Of Jan                   | Jan                           | 978-0-14-311239-3  | 1                 | MYSTERY           | false      | 2020-04-06          \u001B[0m\r\n" +
                "\u001B[34mDeath In The Andes            | Mario Vargas Llosa            | 978-3-51-839728-2  | 1                 | MYSTERY           | false      | 2019-07-07          \u001B[0m\r\n" +
                "\u001B[34mDigital Fortress              | Dan Brown                     | 978-0-31-218087-4  | 1                 | MYSTERY           | true       | 2018-12-06          \u001B[0m\r\n" +
                "\u001B[34mHet Parfum                    | Patrick Suskind               | 978-9-04-460676-8  | 1                 | MYSTERY           | true       | 2020-03-22          \u001B[0m\r\n" +
                "\u001B[34mLa Ciudad Y Los Perros        | Mario Vargas Llosa            | 978-8-42-046706-1  | 1                 | MYSTERY           | false      | 2018-07-07          \u001B[0m\r\n" +
                "\u001B[34mMatilda                       | Roald Dahl                    | 978-0-14-134679-3  | 1                 | FANTASY           | true       | 2020-08-10          \u001B[0m\r\n" +
                "\u001B[34mOne Hundred Years Of Solitude | Gabriel Garcia Marquez        | 978-0-06-088328-7  | 1                 | MYSTERY           | false      | 2020-01-02          \u001B[0m\r\n" +
                "\u001B[34mThe Lost Symbol               | Dan Brown                     | 978-0-30-774190-5  | 1                 | MYSTERY           | true       | 2019-10-08          \u001B[0m\r\n" +
                "\u001B[34mThe Shadow Of The Wind        | Carlos Ruiz Zafon             | 978-0-14-312639-3  | 1                 | MYSTERY           | false      | 2019-11-18          \u001B[0m\r\n" +
                "\u001B[34m---------------------------------------------------------------------------------------------------------------------------------------------------------\u001B[0m\r\n" +
                "\r\n" +
                "\u001B[34mPress enter to continue.\u001B[0m", getOutput());
    }

    @Test
    void showBook() {
        assertThrows(BookException.class, () -> ShowBooks.showBook(null));

        ShowBooks.showBook("getFirstBook");
        assertTrue(getOutput().contains("Moby Dick"));

        resetStreams();
        ShowBooks.showBook("getFirstArchivedBook");
        assertTrue(getOutput().contains("No book found"));
    }

    @Test
    void showBookByIsbnOrTitle() {
        assertThrows(BookException.class, () -> ShowBooks.showBookByIsbnOrTitle(null));
        assertThrows(BookException.class, () -> ShowBooks.showBookByIsbnOrTitle("author"));

        setInput("9785263256452");
        ShowBooks.showBookByIsbnOrTitle("isbn");
        assertTrue(getOutput().contains("No book found"));

        resetStreams();
        setInput("Hhouse");
        ShowBooks.showBookByIsbnOrTitle("title");
        assertTrue(getOutput().contains("No book found"));

        setInput("9780375432309");
        ShowBooks.showBookByIsbnOrTitle("isbn");
        assertTrue(getOutput().contains("The Da Vinci Code"));

        resetStreams();
        setInput("house");
        ShowBooks.showBookByIsbnOrTitle("title");
        assertTrue(getOutput().contains("The House Of The Spirits"));
    }

    @Test
    void showBooksByAuthor() {

        setInput("abc");
        ShowBooks.showBooksByAuthor();
        assertTrue(getOutput().contains("No books found"));

        resetStreams();
        setInput("da");
        ShowBooks.showBooksByAuthor();
        assertTrue(getOutput().contains("Dan Brown"));
        assertTrue(getOutput().contains("Daniel Defoe"));
    }

    @Test
    void showBooksByGenre() {

        setInput("0");
        addInputLine("5");
        ShowBooks.showBooksByGenre();
        assertEquals("Input must be a number between 1 and 9\n", getError());
        assertTrue(getOutput().contains("Dan Brown"));

        setInput("1");
        ShowBooks.showBooksByGenre();
        assertTrue(getOutput().contains("The Hobbit"));
    }

    @Test
    void showRandomBook() {
        ShowBooks.showRandomBook();
        assertTrue(getOutput().contains("Title"));
        assertTrue(getOutput().contains("Author"));
        assertTrue(getOutput().contains("ISBN"));
        assertTrue(getOutput().contains("Revision Nr"));
        assertTrue(getOutput().contains("Genre"));
        assertTrue(getOutput().contains("Fiction"));

        // deleting all the books so that book == null
        em.getTransaction().begin();
        Query query = em.createQuery("delete from Book");
        query.executeUpdate();
        em.getTransaction().commit();

        ShowBooks.showRandomBook();
        assertTrue(getOutput().contains("No random book found"));
    }

    @Test
    void showAllBooksOrderedByTitleAndAuthor() {
        ShowBooks.showAllBooksOrderedByTitleAndAuthor();
        assertTrue(getOutput().contains("Moby Dick"));
        assertTrue(getOutput().contains("Gulliver's Travels"));
        assertTrue(getOutput().contains("Robinson Crusoe"));
        assertTrue(getOutput().contains("The Little Prince"));
        assertTrue(getOutput().contains("The Hobbit"));

        // deleting all the books so that books == null
        em.getTransaction().begin();
        Query query = em.createQuery("delete from Book");
        query.executeUpdate();
        em.getTransaction().commit();

        resetStreams();
        ShowBooks.showAllBooksOrderedByTitleAndAuthor();
        assertTrue(getOutput().contains("No books found"));
    }

    @Test
    void showArchivedBooksOrderedByTitle() {
        ShowBooks.showArchivedBooksOrderedByTitle();
        assertTrue(getOutput().contains("Book Of Jan"));
        assertTrue(getOutput().contains("Digital Fortress"));
        assertTrue(getOutput().contains("Death In The Andes"));
        assertTrue(getOutput().contains("La Ciudad Y Los Perros"));
        assertTrue(getOutput().contains("1984"));

        // deleting all the archived books so that books == null
        em.getTransaction().begin();
        Query query = em.createQuery("delete from ArchivedBook");
        query.executeUpdate();
        em.getTransaction().commit();

        resetStreams();
        ShowBooks.showArchivedBooksOrderedByTitle();
        assertTrue(getOutput().contains("No books found"));
    }

    @Test
    void showArchivedBooksOrderedByDateArchived() {
        ShowBooks.showArchivedBooksOrderedByTitle();
        assertTrue(getOutput().contains("Book Of Jan"));
        assertTrue(getOutput().contains("Digital Fortress"));
        assertTrue(getOutput().contains("Death In The Andes"));
        assertTrue(getOutput().contains("La Ciudad Y Los Perros"));
        assertTrue(getOutput().contains("1984"));

        // deleting all the archived books so that archivedBooks == null
        em.getTransaction().begin();
        Query query = em.createQuery("delete from ArchivedBook");
        query.executeUpdate();
        em.getTransaction().commit();

        resetStreams();
        ShowBooks.showArchivedBooksOrderedByTitle();
        assertTrue(getOutput().contains("No books found"));
    }

    @Test
    void showBooks() {
        assertThrows(BookException.class, () -> ShowBooks.showBooks(null));

        List<Book> books;

        books = bookDao.getBooks("getAllBooks");
        ShowBooks.showBooks(books);
        assertTrue(getOutput().contains("Moby Dick"));
        assertTrue(getOutput().contains("Gulliver's Travels"));
        assertTrue(getOutput().contains("Robinson Crusoe"));
        assertTrue(getOutput().contains("The Little Prince"));
        assertTrue(getOutput().contains("The Hobbit"));

        // deleting all the archived books so that books == null
        em.getTransaction().begin();
        Query query = em.createQuery("delete from Book");
        query.executeUpdate();
        em.getTransaction().commit();

        resetStreams();
        books = bookDao.getBooks("getAllBooks");
        ShowBooks.showBooks(books);
        assertFalse(getOutput().contains("Moby Dick"));
        assertFalse(getOutput().contains("Gulliver's Travels"));
        assertFalse(getOutput().contains("Robinson Crusoe"));
        assertFalse(getOutput().contains("The Little Prince"));
        assertFalse(getOutput().contains("The Hobbit"));
    }

    @Test
    void showSortedBooks() {
        assertThrows(BookException.class, () -> ShowBooks.showSortedBooks(null));

        Comparator<Book> comparator = Comparator.comparing(Book::getGenre);
        ShowBooks.showSortedBooks(comparator);
        assertTrue(getOutput().contains("Moby Dick"));
        assertTrue(getOutput().contains("Gulliver's Travels"));
        assertTrue(getOutput().contains("Robinson Crusoe"));
        assertTrue(getOutput().contains("The Little Prince"));
        assertTrue(getOutput().contains("The Hobbit"));

        // deleting all the archived books so that books == null
        em.getTransaction().begin();
        Query query = em.createQuery("delete from Book");
        query.executeUpdate();
        em.getTransaction().commit();

        ShowBooks.showSortedBooks(comparator);
        assertTrue(getOutput().contains("No books found"));
    }

    @Test
    void showArchivedBooks() {
        assertThrows(BookException.class, () -> ShowBooks.showArchivedBooks(null));

        List<ArchivedBook> archivedBooks;

        archivedBooks = archivedBookDao.getArchivedBooks("getAllArchivedBooks");
        ShowBooks.showArchivedBooks(archivedBooks);
        assertTrue(getOutput().contains("Book Of Jan"));
        assertTrue(getOutput().contains("Digital Fortress"));
        assertTrue(getOutput().contains("Death In The Andes"));
        assertTrue(getOutput().contains("La Ciudad Y Los Perros"));
        assertTrue(getOutput().contains("1984"));

        // deleting all the archived books so that archivedBooks == null
        em.getTransaction().begin();
        Query query = em.createQuery("delete from ArchivedBook");
        query.executeUpdate();
        em.getTransaction().commit();

        resetStreams();
        archivedBooks = archivedBookDao.getArchivedBooks("getAllArchivedBooks");
        ShowBooks.showArchivedBooks(archivedBooks);
        assertFalse(getOutput().contains("Book Of Jan"));
        assertFalse(getOutput().contains("Digital Fortress"));
        assertFalse(getOutput().contains("Death In The Andes"));
        assertFalse(getOutput().contains("La Ciudad Y Los Perros"));
        assertFalse(getOutput().contains("1984"));
    }

    @Test
    void showBooksOrNot() {
        assertThrows(BookException.class, () -> ShowBooks.showBooksOrNot(null));

        List<Book> books;

        books = new ArrayList<>();
        ShowBooks.showBooksOrNot(books);
        assertTrue(getOutput().contains("No books found"));

        books.add(new Book("Book Test", "Author Test", "9786523652145", 1, Genres.SCI_FI, true));
        ShowBooks.showBooksOrNot(books);
        assertTrue(getOutput().contains("Book Test"));
    }

    @Test
    void showArchivedBooksOrNot() {
        assertThrows(BookException.class, () -> ShowBooks.showArchivedBooksOrNot(null));

        List<ArchivedBook> archivedBooks;

        archivedBooks = new ArrayList<>();
        ShowBooks.showArchivedBooksOrNot(archivedBooks);
        assertTrue(getOutput().contains("No books found"));

        Book book = new Book("Archived Book Test", "Author Test", "9785236521458", 1, Genres.FANTASY, true);
        archivedBooks.add(new ArchivedBook(book));
        ShowBooks.showArchivedBooksOrNot(archivedBooks);
        assertTrue(getOutput().contains("Archived Book Test"));
    }
}