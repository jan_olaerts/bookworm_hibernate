package be.janolaerts.bookworm.tools;

import be.janolaerts.bookworm.genres.Genres;
import be.janolaerts.bookworm.testTools.SystemInOutTester;
import org.junit.jupiter.api.Test;

import static be.janolaerts.bookworm.tools.PrintTool.ANSI_BLUE;
import static be.janolaerts.bookworm.tools.PrintTool.ANSI_RESET;
import static org.junit.jupiter.api.Assertions.*;

class UserInputToolTest extends SystemInOutTester {
    private static final String DEFAULT_QUESTION_Y_N = "Question";

    public UserInputToolTest(){
        super();
        UserInputTool.getNewScanner();
    }

    @Test
    protected void askPressEnterToContinue() {
        setInput("");
        UserInputTool.askPressEnterToContinue();
        assertEquals("\u001B[34mPress enter to continue.\u001B[0m", getOutput());
    }

    @Test
    protected void askYesOrNo() {
        assertFalse(UserInputTool.askYesOrNo(null));
        assertFalse(UserInputTool.askYesOrNo(""));

        // input: "y"
        setInput("y");
        assertTrue(UserInputTool.askYesOrNo(DEFAULT_QUESTION_Y_N));
        assertTrue(getOutput().contains(DEFAULT_QUESTION_Y_N));
        assertTrue(getError().isBlank());

        // input: "n"
        resetStreams();
        setInput("n");
        assertFalse(UserInputTool.askYesOrNo(DEFAULT_QUESTION_Y_N));
        assertTrue(getOutput().contains(DEFAULT_QUESTION_Y_N));
        assertTrue(getError().isBlank());

        // input: "Y"
        resetStreams();
        setInput("Y");
        assertTrue(UserInputTool.askYesOrNo(DEFAULT_QUESTION_Y_N));
        assertTrue(getOutput().contains(DEFAULT_QUESTION_Y_N));
        assertTrue(getError().isBlank());

        // input: "n"
        resetStreams();
        setInput("N");
        assertFalse(UserInputTool.askYesOrNo(DEFAULT_QUESTION_Y_N));
        assertTrue(getOutput().contains(DEFAULT_QUESTION_Y_N));
        assertTrue(getError().isBlank());

        // input: "J"
        resetStreams();
        setInput("J");
        addInputLine("y");
        assertTrue(UserInputTool.askYesOrNo(DEFAULT_QUESTION_Y_N));
        assertTrue(getOutput().contains(DEFAULT_QUESTION_Y_N));
        assertEquals("Type in y or n", getError().replace("\n", ""));

        // input: "no"
        resetStreams();
        setInput("no");
        addInputLine("n");
        assertFalse(UserInputTool.askYesOrNo(DEFAULT_QUESTION_Y_N));
        assertTrue(getOutput().contains(DEFAULT_QUESTION_Y_N));
        assertEquals("Answer cannot be longer than 1 char", getError().replace("\n", ""));
    }

    @Test
    protected void askUserIntWithLowBoundary() {
        assertThrows(IllegalArgumentException.class, () -> UserInputTool.askUserIntWithLowBoundary(null, 5));

        resetStreams();
        setInput("abc3");
        addInputLine("6");
        UserInputTool.askUserIntWithLowBoundary("Number at least 2", 2);
        assertTrue(getOutput().contains("Input must be a digit which is equal or higher than 2"));

        resetStreams();
        setInput("-15");
        addInputLine("-6");
        UserInputTool.askUserIntWithLowBoundary("Number at least -10", -10);
        assertTrue(getOutput().contains("Input must be a digit which is equal or higher than -10"));

        resetStreams();
        setInput("5");
        assertEquals(5, UserInputTool.askUserIntWithLowBoundary("Number at least 5", 5));
    }

    @Test
    protected void askUserPosIntBetweenRange() {
        // validation tests
        assertThrows(IllegalArgumentException.class, () -> UserInputTool.askUserPosIntBetweenRange("Int between 9 and 5", 9, 5));
        assertThrows(IllegalArgumentException.class, () -> UserInputTool.askUserPosIntBetweenRange(null, 5, 9));
        assertThrows(IllegalArgumentException.class, () -> UserInputTool.askUserPosIntBetweenRange("Int between -5 and 6", -5, 6));

        // functionality tests
        resetStreams();
        setInput("2");
        addInputLine("5");
        UserInputTool.askUserPosIntBetweenRange("Int between 3 and 6", 3, 6);
        assertEquals("Input must be a number between 3 and 6", getError().replace("\n", ""));

        resetStreams();
        setInput("5");
        assertEquals(5, UserInputTool.askUserPosIntBetweenRange("Int between 3 and 6", 3, 6));

        resetStreams();
        setInput("6");
        assertEquals(6, UserInputTool.askUserPosIntBetweenRange("Int between 6 and 7", 6, 7));
    }

    @Test
    protected void askUserIntBetweenRange() {
        assertThrows(IllegalArgumentException.class, () -> UserInputTool.askUserIntBetweenRange(null, 5, 9));
        assertThrows(IllegalArgumentException.class, () -> UserInputTool.askUserIntBetweenRange("5-2", 5, 2));

        setInput("8");
        setInput("abc");
        addInputLine("3");
        UserInputTool.askUserIntBetweenRange("-5, 4", -5, 4);
        assertEquals("Input must be a number between -5 and 4\n", getError());

        resetStreams();
        setInput("abcd");
        addInputLine("150");
        UserInputTool.askUserIntBetweenRange("149, 150", 149, 150);
        assertEquals("Input must be a number between 149 and 150\n", getError());
    }

    @Test
    protected void askUserString() {
        assertThrows(IllegalArgumentException.class, () -> UserInputTool.askUserString(null));

        resetStreams();
        setInput("This is a string: ");
        assertEquals("This is a string: ", UserInputTool.askUserString("give a string: "));
    }

    @Test
    protected void askUserIsbn() {
        assertThrows(IllegalArgumentException.class, () -> UserInputTool.askUserIsbn(null));

        resetStreams();
        setInput("dfgsd");
        addInputLine("9786325421225");
        UserInputTool.askUserIsbn("Isbn: ");
        assertEquals("\rInvalid isbn\n", getError());

        resetStreams();
        addInputLine("9782145236524");
        UserInputTool.askUserIsbn("Isbn: ");
        assertEquals(ANSI_BLUE + "Isbn: " + ANSI_RESET, getOutput());

        resetStreams();
        addInputLine("9786325421");
        assertEquals("9-78-632542-1", UserInputTool.askUserIsbn("Isbn: "));
    }

    @Test
    protected void askUserBookGenre() {
        assertThrows(IllegalArgumentException.class, () -> UserInputTool.askUserBookGenre(null));

        resetStreams();
        setInput("15");
        addInputLine("3");
        UserInputTool.askUserBookGenre("Genre: ");
        assertEquals("Input must be a number between 1 and 9\n", getError());

        resetStreams();
        setInput("abc");
        addInputLine("5");
        UserInputTool.askUserBookGenre("Genre: ");
        assertEquals("Input must be a number between 1 and 9\n", getError());

        addInputLine("1");
        assertEquals(Genres.ADVENTURE, UserInputTool.askUserBookGenre("Genre: "));
    }

    @Test
    protected void askIsbnAndCheckIfInDb() {

        // If method gives String back isbn was not in db
        addInputLine("978-0-00-105243-0");
        assertEquals("978-0-00-105243-0", UserInputTool.askIsbnAndCheckIfInDb());
    }
}