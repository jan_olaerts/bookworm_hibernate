package be.janolaerts.bookworm.tools;

import be.janolaerts.bookworm.testTools.SystemInOutTester;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class StringToolTest extends SystemInOutTester {

    @Test
    void insertCharInString() {

        assertThrows(IllegalArgumentException.class, () -> StringTool.insertCharInString(null, '#', new ArrayList<>(List.of(2))));
        assertThrows(IllegalArgumentException.class, () -> StringTool.insertCharInString("abc", '-', null));

        assertEquals("abc-", StringTool.insertCharInString("abc", '-', new ArrayList<>(List.of(3))));

        assertEquals("abc-def-ghi-",
                StringTool.insertCharInString("abcdefghi", '-', new ArrayList<>(List.of(3, 6, 9))));

        assertEquals("abc-def-ghi-",
                StringTool.insertCharInString("abcdefghi", '-', new ArrayList<>(List.of(9, 6, 3))));

        assertEquals("abcddefdghid",
                StringTool.insertCharInString("abcdefghi", 'd', new ArrayList<>(List.of(3, 6, 9))));

        assertEquals("978-3-16-148410-0",
                StringTool.insertCharInString("9783161484100", '-', new ArrayList<>(List.of(3, 4, 6, 12))));

        assertNull(StringTool.insertCharInString("abc", '-', new ArrayList<>(List.of(2, 4))));
    }

    @Test
    void filterDigitsFromString() {
        assertThrows(IllegalArgumentException.class, () -> StringTool.filterDigitsFromString(null));

        assertEquals("856", StringTool.filterDigitsFromString("abc856-[]d&e$f^"));
        assertEquals("123456789", StringTool.filterDigitsFromString("123abc456def789ghi"));
        assertEquals("0198526636", StringTool.filterDigitsFromString("0198526636"));
    }

    @Test
    void checkIsbn() {
        assertThrows(IllegalArgumentException.class, () -> StringTool.checkIsbn(null));

        assertTrue(StringTool.checkIsbn("0198526636"));
        assertTrue(StringTool.checkIsbn("0198&526^63]6"));
        assertTrue(StringTool.checkIsbn("9783161484100"));
        assertTrue(StringTool.checkIsbn("97G8316=148(410%0"));
        assertFalse(StringTool.checkIsbn("1783161484100"));

        assertFalse(StringTool.checkIsbn("123"));
        assertTrue(getError().contains("Invalid isbn"));
    }

    @Test
    void putDashesInIsbnString() {
        assertThrows(IllegalArgumentException.class, () -> StringTool.putDashesInIsbnString(null));

        assertEquals("01234567891", StringTool.putDashesInIsbnString("01234567891"));
        assertEquals("01234567891", StringTool.putDashesInIsbnString("0-1g2?3&4^5%6(7)891"));
        assertEquals("0-12-345678-9", StringTool.putDashesInIsbnString("0-1g2?3&4^5%6(7)89"));
        assertEquals("012-3-45-678901-2", StringTool.putDashesInIsbnString("012-34-56-789-01-2"));
    }

    @Test
    void capitalizeFirstLetterOfEachWord() {
        assertThrows(IllegalArgumentException.class, () -> StringTool.capitalizeFirstLetterOfEachWord(null));

        assertEquals("1abc", StringTool.capitalizeFirstLetterOfEachWord("1abc"));
        assertEquals("#abc &123", StringTool.capitalizeFirstLetterOfEachWord("#abc &123"));
        assertEquals("#abc Abc Abc", StringTool.capitalizeFirstLetterOfEachWord("#abc abc-abc"));
        assertEquals("Title Of A Book", StringTool.capitalizeFirstLetterOfEachWord("title-of-a-book"));
    }
}