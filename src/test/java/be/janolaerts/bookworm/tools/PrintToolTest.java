package be.janolaerts.bookworm.tools;

import be.janolaerts.bookworm.testTools.SystemInOutTester;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static be.janolaerts.bookworm.tools.PrintTool.ANSI_BLUE;
import static be.janolaerts.bookworm.tools.PrintTool.ANSI_RESET;

class PrintToolTest extends SystemInOutTester {

    @Test
    void printBlue() {
        assertThrows(IllegalArgumentException.class, () -> PrintTool.printBlue(null));

        PrintTool.printBlue("This is blue text");
        assertEquals(ANSI_BLUE + "This is blue text" + ANSI_RESET, getOutput());
    }

    @Test
    void printBlueAndEnter() {
        assertThrows(IllegalArgumentException.class, () -> PrintTool.printBlueAndEnter(null));

        PrintTool.printBlue("This is blue text");
        assertEquals(ANSI_BLUE + "This is blue text" + ANSI_RESET, getOutput());
    }

    @Test
    void printYellow() {
        assertThrows(IllegalArgumentException.class, () -> PrintTool.printBlue(null));

        PrintTool.printBlue("This is yellow text");
        assertEquals(ANSI_BLUE + "This is yellow text" + ANSI_RESET, getOutput());
    }

    @Test
    void printYellowAndEnter() {
        assertThrows(IllegalArgumentException.class, () -> PrintTool.printBlue(null));

        PrintTool.printBlue("This is yellow text");
        assertEquals(ANSI_BLUE + "This is yellow text" + ANSI_RESET, getOutput());
    }

    @Test
    void printEnter() {
        PrintTool.printEnter();
        assertEquals("\r\n", getOutput());
    }

    @Test
    void printBookHeading() {
        PrintTool.printBookHeading();
        assertEquals(ANSI_BLUE + "Title                         | Author                        | ISBN               | Revision Nr       | Genre             | Fiction             " + ANSI_RESET + "\r\n" +
                ANSI_BLUE + "------------------------------------------------------------------------------------------------------------------------------------" + ANSI_RESET + "\r\n", getOutput());
    }

    @Test
    void printArchivedBookHeading() {
        PrintTool.printArchivedBookHeading();
        assertEquals(ANSI_BLUE + "Title                         | Author                        | ISBN               | Revision Nr       | Genre             | Fiction    | Archived Date       " + ANSI_RESET + "\r\n" +
                ANSI_BLUE + "---------------------------------------------------------------------------------------------------------------------------------------------------------" + ANSI_RESET + "\r\n", getOutput());
    }
}