package be.janolaerts.bookworm.tools;

import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.*;

class HibernateToolTest {

    @Test
    public void getEntityManagerTest() {
        EntityManager em = HibernateTool.getEntityManager();
        assertNotNull(em);
        assertTrue(em.toString().contains("open"));
        HibernateTool.closeEntityManager();
    }

    @Test
    public void closeEntityManagerTest() {
        EntityManager em = HibernateTool.getEntityManager();
        assertNotNull(em);
        HibernateTool.closeEntityManager();
        assertTrue(em.toString().contains("closed"));
    }

    @Test
    public void closeEntityManagerFactoryTest() {
        EntityManager em = HibernateTool.getEntityManager();
        assertNotNull(em);
        assertNotNull(HibernateTool.emf);
        HibernateTool.closeEntityManagerFactory();
        assertNull(HibernateTool.emf);
    }
}