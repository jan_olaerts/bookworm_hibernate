package be.janolaerts.bookworm.tools;

import be.janolaerts.bookworm.exception.BookException;
import be.janolaerts.bookworm.genres.Genres;
import be.janolaerts.bookworm.daos.BookDao;
import be.janolaerts.bookworm.entities.Book;

import java.util.InputMismatchException;
import java.util.Scanner;

import static be.janolaerts.bookworm.tools.PrintTool.*;
import static be.janolaerts.bookworm.tools.StringTool.*;

/**
 * @author Jan
 * This class handles all the user input
 */
public class UserInputTool {

    private static Scanner scanner = new Scanner(System.in);
    private static final BookDao bookDao = new BookDao();

    /**
     * This method lets the scanner use the same stream
     */
    public static void getNewScanner(){
        scanner = new Scanner(System.in);
    }

    /**
     * Asking for the enter if the user wants to continue
     */
    public static void askPressEnterToContinue() {
        printBlue("Press enter to continue.");
        scanner.nextLine();
    }

    /**
     * Asks yes or no question
     * @param message is a string that you are asking to user for yes no question
     * @return is the answer of the user in type of boolean true or false
     */
    public static boolean askYesOrNo(String message) {
        if (message == null || message.length() < 1) return false;
        String answer;
        do {
            printBlue(message);
            answer = scanner.nextLine().toLowerCase();
            if (answer.length() > 1) {
                System.err.println("Answer cannot be longer than 1 char");
                printEnter();
            } else if (!answer.equals("y") && !answer.equals("n")) {
                System.err.println("Type in y or n");
                printEnter();
            }
        } while (!answer.equals("y") && !answer.equals("n"));

        return answer.equals("y");
    }

    /**
     * This method asks the user for a number which is higher or equal than a boundary
     * @param message is the message we want to print for the user
     * @param low is the low boundary
     * @return a number the user put in
     * @throws IllegalArgumentException if the message is null
     */
    public static int askUserIntWithLowBoundary(String message, int low) {
        if(message == null) throw new IllegalArgumentException("message cannot be null");
        String errMessage = "Input must be a digit which is equal or higher than " + low;

        int num  = low-1;
        do {

            try {
                printBlue(message);
//                num = scanner.nextInt();
                String input = scanner.nextLine();
                num = Integer.parseInt(input);
                if(num < low) {
                    printEnter();
                    printYellowAndEnter(errMessage);
                }
                if(num >= low) break;
            } catch (Exception ex) {
                printEnter();
                printYellowAndEnter(errMessage);
            }
        } while (true);

        return num;
    }

    /**
     * Method for asking user to enter between 2 boundaries
     * @param message is your question in type of string
     * @param low is your lowest boundary for user's answer can give
     * @param high is your highest boundary for user's answer can give
     * @return the answer of the user if all in order in type of int
     * @throws IllegalArgumentException if low is smaller than 0
     * @throws IllegalArgumentException if low is greater than high
     * @throws IllegalArgumentException if message is null
     */
    public static int askUserPosIntBetweenRange(String message, int low, int high) {
        if (low < 0) throw new IllegalArgumentException("low cannot be negative");
        if (low > high) throw new IllegalArgumentException("The lower end cannot be greater than the higher end");
        if (message == null) throw new IllegalArgumentException("The message cannot be null");

        String input;
        String regex = "[" + low + "-" + high + "]";

        if(high > 9) return askUserIntBetweenRange(message, low, high);
        do {
            printBlue(message);
            input = scanner.nextLine();
            if (!input.matches(regex)) {
                System.err.println("Input must be a number between " + low + " and " + high);
                printEnter();
            }
        } while (!input.matches(regex));

        printEnter();
        return Integer.parseInt(input);
    }

    /**
     * This method asks the user for a number in a given range
     * @param message is the message we print for the user
     * @param low is the low end of the range
     * @param high is the high end of the range
     * @return the number the user put in
     * @throws IllegalArgumentException if the low end is higher than the high end
     * @throws IllegalArgumentException if the message is null
     */
    public static int askUserIntBetweenRange(String message, int low, int high) {
        if (low > high) throw new IllegalArgumentException("The lower end cannot be greater than the higher end");
        if (message == null) throw new IllegalArgumentException("The message cannot be null");

        String errMessage = "Input must be a number between " + low + " and " + high;
        int num = low -1;
        do {

            try {
                printBlue(message);
                num = scanner.nextInt();
                if(num < low || num > high) {
                    System.err.println(errMessage);
                    printEnter();
                }
            } catch (InputMismatchException imme) {
                System.err.println(errMessage);
                printEnter();
            } finally {
                scanner.nextLine();
            }
        } while (num < low || num > high);

        return num;
    }

    /**
     * This method asks the user for a string
     * @param message is the message we print for the user
     * @return the string the user put in
     * @throws IllegalArgumentException if the message is null
     */
    public static String askUserString(String message) {
        if(message == null) throw new IllegalArgumentException("message cannot be null");

        String input;
        do {
            printBlue(message);
            input = scanner.nextLine();
            if(input.length() == 0) {
                System.err.println("Input is empty");
                printEnter();
            }
        } while (input.length() == 0);

        return input;
    }

    /**
     * Asks the user for an isbn
     * @param message is the message we print for the user
     * @return the altered isbn with dashes
     * @throws IllegalArgumentException if the message is null
     */
    public static String askUserIsbn(String message) {
        if(message == null) throw new IllegalArgumentException("message cannot be null");

        String isbn;
        do {
            isbn = askUserString(message);
        } while(!StringTool.checkIsbn(isbn));

        return putDashesInIsbnString(isbn);
    }

    /**
     * This method asks the user for a book genre
     * @param message is the message we print for the user
     * @return a Genre type
     * @throws IllegalArgumentException if the message is null
     */
    public static Genres askUserBookGenre(String message) {
        if(message == null) throw new IllegalArgumentException("message cannot be null");
        Genres[] genres = Genres.values();

        int count = 1;
        for(Genres genre : genres)
            printBlueAndEnter(count++ + "| " + genre);

        int choice = askUserPosIntBetweenRange(message, 1, genres.length);
        return genres[choice -1];
    }

    /**
     * This method asks the user for an isbn and check wheter the isbn is in the db
     * @return The isbn the user put in
     */
    public static String askIsbnAndCheckIfInDb() {

        Book book;
        String isbn = null;
        do {
            try {
                isbn = askUserIsbn("Isbn: ");
                book = bookDao.getBook("getBookByIsbn", isbn);
                if(book != null) {
                    System.err.println("Isbn is already in db");
                    printEnter();
                }
            } catch (BookException be) {
                break;
            }
        } while(book != null);

        return isbn;
    }
}