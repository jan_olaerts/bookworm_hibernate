package be.janolaerts.bookworm.tools;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * @author Jan
 * Class to do Hibernate-specific actions
 */
public class HibernateTool {

    public static EntityManagerFactory emf = null;
    public static EntityManager em = null;

    /**
     * Gets an EntityManager object
     * @return an EntityManager object
     */
    public static EntityManager getEntityManager() {

        try {

            if(emf == null) {
                emf = Persistence.createEntityManagerFactory("bookworm");
            }

            em = emf.createEntityManager();

        } catch (RuntimeException re) {
            System.out.println("Could not create entity manager " + re.getMessage());
            re.printStackTrace();
        }

        return em;
    }

    /**
     * Closes the EntityManager object
     */
    public static void closeEntityManager() {

        if(em != null) {
            em.close();
            em = null;
        }
    }

    /**
     * Closes the EntityManagerFactory object
     */
    public static void closeEntityManagerFactory() {

        if(emf != null) {
            emf.close();
            emf = null;
        }
    }
}