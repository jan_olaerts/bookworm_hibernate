package be.janolaerts.bookworm.tools;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Jan
 * This class handles all the logic for working with String objects
 */
public class StringTool {

    /**
     * This method insert given chars in a given string at given indexes
     * @param string is the string we want to add chars to
     * @param type are the chars we want to add
     * @param indexes are the indexes we want to put the chars in
     * @return the altered string with the given chars added to at the given indexes
     * @throws IllegalArgumentException if the string is null
     * @throws IllegalArgumentException if the indexes list is null
     * @throws IllegalArgumentException if the indexes list does not have content
     */
    public static String insertCharInString(String string, char type, List<Integer> indexes) {
        if(string == null || indexes == null || indexes.size() < 1)
            throw new IllegalArgumentException("string cannot be null, indexes cannot be null and indexes must contain data");

        Collections.sort(indexes);
        StringBuilder sb = new StringBuilder(string);

        try {
            int count = 0;
            for(Integer index : indexes) {
                if (index == null || index > string.length())
                    throw new IllegalArgumentException("index is null or index is larger than string");
                sb.insert(index + count, type);
                count++;
            }
        } catch (IllegalArgumentException iae) {
            return null;
        }

        return sb.toString();
    }

    /**
     * This method filters all non-digit chars out off a string
     * @param input is the string we want to remove no-digits from
     * @return the string where the non-digits chars are filtered from
     * @throws IllegalArgumentException if the input is null
     */
    public static String filterDigitsFromString(String input) {
        if(input == null) throw new IllegalArgumentException("input cannot be null");

        return input.replaceAll("[^\\d]", "");
    }

    /**
     * This method checks whether a given isbn is a valid one
     * @param isbn is the isbn we want to check
     * @return true if the isbn is valid and false if the isbn is not valid
     * @throws IllegalArgumentException if the isbn is null
     */
    public static boolean checkIsbn(String isbn) {
        if(isbn == null) throw new IllegalArgumentException("isbn cannot be null");

        isbn = filterDigitsFromString(isbn);

        if(isbn.length() == 10) {

            isbn = insertCharInString(isbn, '-', new ArrayList<>(List.of(1, 3, 9)));
            if(isbn.matches("^[\\d]-[\\d]{2}-[\\d]{6}-[\\d]$")) return true;
        } else if (isbn.length() == 13) {
            isbn = insertCharInString(isbn, '-', new ArrayList<>(List.of(3, 4, 6, 12)));
            if(isbn.matches("^97[89]-[\\d]-[\\d]{2}-[\\d]{6}-[\\d]$")) return true;
        }

        System.err.print("\rInvalid isbn\n");
        PrintTool.printEnter();

        return false;
    }

    /**
     * This method puts dashes in a given isbn string
     * @param isbn is the isbn we want to put dashes to
     * @return the isbn with dashes added to
     * @throws IllegalArgumentException if the isbn is null
     */
    public static String putDashesInIsbnString(String isbn) {
        if(isbn == null) throw new IllegalArgumentException("isbn cannot be null");

        boolean tenRan = false;
        isbn = filterDigitsFromString(isbn);

        if(isbn.length() == 10) {
            isbn = insertCharInString(isbn, '-', new ArrayList<>(List.of(1, 3, 9)));
            tenRan = true;
        }

        if(isbn.length() == 13 && !tenRan)
            isbn = insertCharInString(isbn, '-', new ArrayList<>(List.of(3, 4, 6, 12)));

        return isbn;
    }

    /**
     * This method capitalizes the first char of a given text
     * @param text is the text we want to capitalize the first char of
     * @return the string with the first char capitalized of each word
     * @throws IllegalArgumentException if the given text is null
     */
    public static String capitalizeFirstLetterOfEachWord(String text) {
        if(text == null) throw new IllegalArgumentException("text cannot be null");

        StringBuilder sb = new StringBuilder();
        if(text.length() > 0) {
            String[] words = text.trim().toLowerCase().split("[ -]");
            for(String w : words) {
                sb.append(Character.toUpperCase(w.charAt(0)))
                        .append(w.substring(1))
                        .append(" ");
            }
        }

        return sb.toString().trim();
    }
}