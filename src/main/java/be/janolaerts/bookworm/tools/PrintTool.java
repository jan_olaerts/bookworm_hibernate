package be.janolaerts.bookworm.tools;

import be.janolaerts.bookworm.entities.Book;
import be.janolaerts.bookworm.exception.BookException;

/**
 * @author Jan
 * This class handles all the printing
 */
public class PrintTool {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";

    /**
     * Prints a given string in blue
     * @param text string to be printed in blue
     */
    public static void printBlue(String text) {
        if(text == null) throw new IllegalArgumentException("text cannot be null");

        System.out.print(ANSI_BLUE + text + ANSI_RESET);
    }

    /**
     * This method prints a given text in blue followed by an enter
     * @param text is the text we want to print
     * @throws IllegalArgumentException if the text is null
     */
    public static void printBlueAndEnter(String text) {
        if(text == null) throw new IllegalArgumentException("text cannot be null");

        printBlue(text);
        printEnter();
    }

    /**
     * This method prints a given text in yellow
     * @param text is the text we want to print
     * @throws IllegalArgumentException if the text is null
     */
    public static void printYellow(String text) {
        if(text == null) throw new IllegalArgumentException("text cannot be null");

        System.out.print(ANSI_YELLOW + text + ANSI_RESET);
    }

    /**
     * This method prints a given text in yellow followed by an enter
     * @param text is the text we want to print
     * @throws IllegalArgumentException if the text is null
     */
    public static void printYellowAndEnter(String text) {
        if(text == null) throw new IllegalArgumentException("text cannot be null");

        printYellow(text);
        printEnter();
    }

    /**
     * Prints an enter
     */
    public static void printEnter() {
        System.out.println("\r");
    }

    /**
     * This method prints the heading for a Book object
     */
    public static void printBookHeading() {
        String heading = String.format("%-30s| %-30s| %-18s | %-18s| %-18s| %-20s",
                "Title", "Author", "ISBN", "Revision Nr", "Genre", "Fiction");
        printBlueAndEnter(heading);
        printBlueAndEnter("-------------------------------------------------------" +
                "-----------------------------------------------------------------------------");
    }

    /**
     * This method prints the heading for an ArchivedBook object
     */
    public static void printArchivedBookHeading() {
        String heading = String.format("%-30s| %-30s| %-18s | %-18s| %-18s| %-10s | %-20s",
                "Title", "Author", "ISBN", "Revision Nr", "Genre", "Fiction", "Archived Date");
        printBlueAndEnter(heading);
        printBlueAndEnter("-------------------------------------------------------" +
                "-----------------------------------------------------------------------------" +
                "---------------------");
    }

    /**
     * This method prints a Book object with separations
     * @param book is the Book object we want to print
     */
    public static void printBook(Book book) {
        if(book == null) throw new BookException("book cannot be null");

        printBlueAndEnter("\n-------------------------------------------------------" +
                "-----------------------------------------------------------------------------");
        printBookHeading();
        printBlueAndEnter(book.toString());
        printBlueAndEnter("-------------------------------------------------------" +
                "-----------------------------------------------------------------------------");
        printEnter();
    }
}