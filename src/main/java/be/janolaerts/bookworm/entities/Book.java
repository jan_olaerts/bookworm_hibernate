package be.janolaerts.bookworm.entities;

import be.janolaerts.bookworm.genres.Genres;
import be.janolaerts.bookworm.exception.BookException;
import be.janolaerts.bookworm.tools.StringTool;

import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * @author Jan
 * This class is the Book entity
 */
@Entity
@Table(name="Books")
@NamedQueries(value={
        @NamedQuery(name="getAllBooks", query="select b from Book b"),
        @NamedQuery(name="getBookById", query="select b from Book b where b.id=?1"),
        @NamedQuery(name="getBookByIsbn", query="select b from Book b where b.isbn=?1"),
        @NamedQuery(name="getBookByTitle", query="select b from Book b where b.title like ?1 order by b.id"),
        @NamedQuery(name="getBooksByAuthor", query="select b from Book b where b.author like ?1 order by b.author"),
        @NamedQuery(name="getBooksByGenre", query="select b from Book b where b.genre=?1 order by b.title"),
        @NamedQuery(name="getAllBooksOrderedByTitleAuthor", query="select b from Book b order by b.title, b.author"),
        @NamedQuery(name="getFirstBook", query="select min(b) from Book b"),
        @NamedQuery(name="getLastBook", query="select max(b) from Book b"),
        @NamedQuery(name="getBookCount", query="select count(b) from Book b"),
})
public class Book {

    @Transient
    private static final String ISBN_REGEX =
            "([\\d]-[\\d]{2}-[\\d]{6}-[\\d])|(97[89]-[\\d]-[\\d]{2}-[\\d]{6}-[\\d])";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotBlank
    private String title;

    @NotBlank
    private String author;

    @Pattern(regexp = ISBN_REGEX, message="Not a valid ISBN")
    private String isbn;

    @PositiveOrZero
    private int revisionNr;

    @Enumerated(EnumType.STRING)
    @NotNull
    private Genres genre;

    private boolean fiction;

    public Book() {
    }

    public Book(String title, String author, String isbn, int revisionNr,
                Genres genre, boolean fiction) {

        setTitle(title);
        setAuthor(author);
        setIsbn(isbn);
        setRevisionNr(revisionNr);
        setGenre(genre);
        setFiction(fiction);
    }

    /**
     * Getter for the id field
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * Getter for the title field
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Getter for the author field
     * @return the author
     */
    public String getAuthor() {
        return author;
    }

    /**
     * Getter for the isbn field
     * @return the isbn
     */
    public String getIsbn() {
        return isbn;
    }

    /**
     * Getter for the revision number field
     * @return the revision number
     */
    public int getRevisionNr() {
        return revisionNr;
    }

    /**
     * Getter for the genre field
     * @return the genre
     */
    public Genres getGenre() {
        return genre;
    }

    /**
     * Getter for the fiction field
     * @return whether the book is fiction or not
     */
    public boolean isFiction() {
        return fiction;
    }

    /**
     * Setter for the title field
     * @param title is the title we want to set
     * @throws BookException if the title is null
     */
    public void setTitle(String title) throws BookException {
        if(title == null) throw new BookException("title cannot be null");
        this.title = title;
    }

    /**
     * Setter for the author field
     * @param author is the author we want to set
     * @throws BookException if the author is null
     */
    public void setAuthor(String author) throws BookException {
        if(author == null) throw new BookException("author cannot be null");
        this.author = author;
    }

    /**
     * Setter for the isbn field
     * @param isbn is the isbn we want to set
     * @throws BookException if the isbn is not valid
     */
    public void setIsbn(String isbn) throws BookException {
        if(!StringTool.checkIsbn(isbn)) throw new BookException("Not a valid isbn!");
        isbn = StringTool.putDashesInIsbnString(isbn);

        this.isbn = isbn;
    }

    /**
     * Setter for the revision number field
     * @param revisionNr is the revision number we want to set
     * @throws BookException if the revision number is smaller than 0
     */
    public void setRevisionNr(int revisionNr) throws BookException {
        if(revisionNr < 0) throw new BookException("revisionNr cannot be smaller than 0");
        this.revisionNr = revisionNr;
    }

    /**
     * Setter for the genre field
     * @param genre is the genre we want to set
     * @throws BookException if the genre is null
     */
    public void setGenre(Genres genre) throws BookException {
        if(genre == null) throw new BookException("genre cannot be null");
        this.genre = genre;
    }

    /**
     * Setter for the fiction field
     * @param fiction is a boolean which is the fiction value we want to set
     */
    public void setFiction(boolean fiction) {
        this.fiction = fiction;
    }

    /**
     * Transforms the fields of an Book object in a readable way
     * @return a readable representation of the field of an Book object
     */
    @Override
    public String toString() {
        return String.format("%-30s| %-30s| %-18s | %-18d| %-18s| %-20s",
                            title, author, isbn, revisionNr, genre, fiction);
    }
}