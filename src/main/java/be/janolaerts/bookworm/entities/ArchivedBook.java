package be.janolaerts.bookworm.entities;

import be.janolaerts.bookworm.genres.Genres;
import be.janolaerts.bookworm.exception.BookException;
import be.janolaerts.bookworm.tools.StringTool;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;

/**
 * @author Jan
 * This class is the ArchivedBook entity
 */
@Entity
@Table(name="Archived_Books")
@NamedQueries(value={
        @NamedQuery(name="getAllArchivedBooks", query="select ab from ArchivedBook  ab"),
        @NamedQuery(name="getArchivedBooksOrderedByTitle", query="select ab from ArchivedBook ab order by ab.title"),
        @NamedQuery(name="getArchivedBooksOrderedByArchivedDate",
                query="select ab from ArchivedBook ab order by ab.archivedDate, ab.title"),
        @NamedQuery(name="getFirstArchivedBook", query="select min(ab) from ArchivedBook ab")
})
public class ArchivedBook {

    @Transient
    private static final String ISBN_REGEX =
            "([\\d]-[\\d]{2}-[\\d]{6}-[\\d])|(97[89]-[\\d]-[\\d]{2}-[\\d]{6}-[\\d])";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotBlank
    private String title;

    @NotBlank
    private String author;

    @Pattern(regexp = ISBN_REGEX, message="Not a valid ISBN")
    private String isbn;

    @PositiveOrZero
    private int revisionNr;

    @Enumerated(EnumType.STRING)
    @NotNull
    private Genres genre;

    private boolean fiction;

    @PastOrPresent
    private LocalDate archivedDate;

    public ArchivedBook() {
    }

    public ArchivedBook(Book book) {
        setTitle(book.getTitle());
        setAuthor(book.getAuthor());
        setIsbn(book.getIsbn());
        setRevisionNr(book.getRevisionNr());
        setGenre(book.getGenre());
        setFiction(book.isFiction());
        setArchivedDate(LocalDate.now());
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getIsbn() {
        return isbn;
    }

    public int getRevisionNr() {
        return revisionNr;
    }

    public Genres getGenre() {
        return genre;
    }

    public boolean isFiction() {
        return fiction;
    }

    public LocalDate getArchivedDate() {
        return archivedDate;
    }

    /**
     * Setter for the title
     * @param title is the title we want to set
     * @throws BookException if the title is null
     */
    public void setTitle(String title) throws BookException {
        if(title == null) throw new BookException("title cannot be null");
        this.title = title;
    }

    /**
     * Setter for the author
     * @param author is the author we want to set
     * @throws BookException if the author is null
     */
    public void setAuthor(String author) throws BookException {
        if(author == null) throw new BookException("author cannot be null");
        this.author = author;
    }

    /**
     * Setter for the isbn
     * Adds dashes to the isbn before setting
     * @param isbn is the isbn we want to set
     * @throws BookException if isbn is not valid
     */
    public void setIsbn(String isbn) throws BookException {
        if(!StringTool.checkIsbn(isbn)) throw new BookException("Not a valid isbn!");
        isbn = StringTool.putDashesInIsbnString(isbn);

        this.isbn = isbn;
    }

    /**
     * Setter for the revision number
     * @param revisionNr is the revision number we want to set
     * @throws BookException if the revision number is smaller than 0
     */
    public void setRevisionNr(int revisionNr) throws BookException {
        if(revisionNr < 0) throw new BookException("revisionNr cannot be smaller than 0");
        this.revisionNr = revisionNr;
    }

    /**
     * Setter for the genre
     * @param genre is the genre we want to set
     * @throws BookException if the genre is null
     */
    public void setGenre(Genres genre) throws BookException {
        if(genre == null) throw new BookException("genre cannot be null");
        this.genre = genre;
    }

    /**
     * Setter for the fiction field
     * @param fiction is a boolean for the fiction field
     */
    public void setFiction(boolean fiction) {
        this.fiction = fiction;
    }

    /**
     * Setter for the archivedDate field
     * @param archivedDate is the archivedDate we want to set
     */
    public void setArchivedDate(LocalDate archivedDate) {
        if(archivedDate == null) throw new BookException("archivedDate cannot be null");
        if(archivedDate.isAfter(LocalDate.now())) throw new BookException("archivedDate cannot be in the future");

        this.archivedDate = archivedDate;
    }

    /**
     * Transforms the fields of an ArchivedBook object in a readable way
     * @return a readable representation of the field of an ArchivedBook object
     */
    @Override
    public String toString() {
        return String.format("%-30s| %-30s| %-18s | %-18s| %-18s| %-11s| %-20s",
                title, author, isbn, revisionNr, genre, fiction, archivedDate);
    }
}