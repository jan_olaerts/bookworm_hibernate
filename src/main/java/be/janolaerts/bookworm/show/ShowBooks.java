package be.janolaerts.bookworm.show;

import be.janolaerts.bookworm.daos.ArchivedBookDAO;
import be.janolaerts.bookworm.daos.BookDao;
import be.janolaerts.bookworm.entities.ArchivedBook;
import be.janolaerts.bookworm.entities.Book;
import be.janolaerts.bookworm.exception.BookException;
import be.janolaerts.bookworm.genres.Genres;
import be.janolaerts.bookworm.tools.PrintTool;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static be.janolaerts.bookworm.tools.PrintTool.*;
import static be.janolaerts.bookworm.tools.PrintTool.printEnter;
import static be.janolaerts.bookworm.tools.StringTool.capitalizeFirstLetterOfEachWord;
import static be.janolaerts.bookworm.tools.UserInputTool.*;

/**
 * @author Jan
 * This class lists all the methods for showing books and archived books
 */
public class ShowBooks {

    private static final BookDao bookDao = new BookDao();
    private static final ArchivedBookDAO archivedBookDao = new ArchivedBookDAO();

    /**
     * This method lets the user choose how he wants to show the books
     * @param choice determines the way to show the books
     * @return an int which is the choice
     */
    public static int doShowBooks(int choice) {
        if(choice < 0 || choice > 13) throw new IllegalArgumentException(
                "choice cannot be smaller than 0 or larger than 13");

        switch(choice) {
            case 1:
                showSortedBooks(Comparator.comparingLong(Book::getId));
                break;
            case 2:
                showSortedBooks(Comparator.comparing(Book::getTitle));
                break;
            case 3:
                showSortedBooks(Comparator.comparing(Book::getAuthor));
                break;
            case 4:
                showSortedBooks(Comparator.comparing(Book::getIsbn));
                break;
            case 5:
                showSortedBooks(Comparator.comparing(Book::getGenre));
                break;
            case 6:
                showAllBooksOrderedByTitleAndAuthor();
                break;
            case 7:
                showBookByIsbnOrTitle("isbn");
                break;
            case 8:
                showBookByIsbnOrTitle("title");
                break;
            case 9:
                showBooksByAuthor();
                break;
            case 10:
                showBooksByGenre();
                break;
            case 11:
                showRandomBook();
                break;
            case 12:
                showBook("getFirstBook");
                break;
            case 13:
                showBook("getLastBook");
                break;
            case 0:
                break;
        }

        if(choice != 0) askPressEnterToContinue();
        return choice;
    }

    /**
     * This method determines the way in which we want to view the archived books
     * @param choice selects the way in which we want to view the archived books
     * @return the choice the user entered if it is 0
     */
    public static int doShowArchivedBooks(int choice) {
        if(choice < 0 || choice > 2) throw new BookException("choice must be between 0 and 2");

        switch(choice) {
            case 1:
                showArchivedBooksOrderedByTitle();
                break;
            case 2:
                showArchivedBooksOrderedByDateArchived();
                break;
            case 0:
                break;
        }

        if(choice != 0) askPressEnterToContinue();
        return choice;
    }

    /**
     * This method shows a book
     * @param queryStr is the query by which we look up the book
     */
    public static void showBook(String queryStr) {
        if(queryStr == null) throw new BookException("queryStr cannot be null");

        try {
            Book book = bookDao.getBook(queryStr);
            if(book != null) printBook(book);
            else printBlueAndEnter("No book found");

        } catch (BookException be) {
            printEnter();
            printBlueAndEnter("No book found");
            printEnter();
        }
    }

    /**
     * This method shows a book which is queried by isbn or title
     * @param criteria is the query by which we look up the book
     * @return the queried Book object
     */
    public static Book showBookByIsbnOrTitle(String criteria) {
        if(criteria == null) throw new BookException("criteria cannot be null");
        if(!criteria.equals("isbn") && !criteria.equals("title")) throw new BookException("criteria must be isbn or title");

        Book book = null;
        try {
            printEnter();
            if(criteria.equals("isbn")) {
                String isbn = askUserIsbn("Isbn: ");
                book = bookDao.getBook("getBookByIsbn", isbn);
                if(book != null) printBook(book);
                else printBlueAndEnter("No book found");
            }
            if(criteria.equals("title")) {
                String title = askUserString("Title: ");
                title = capitalizeFirstLetterOfEachWord(title);
                book = bookDao.getBook("getBookByTitle", "%" + title + "%");
                if(book != null) printBook(book);
                else printBlueAndEnter("No book found");
            }
        } catch (BookException be) {
            printEnter();
            printBlueAndEnter("No book found");
            printEnter();
        }

        return book;
    }

    /**
     * This method shows a list of books which are queried by author
     */
    public static void showBooksByAuthor() {

        String author = null;
        try {
            printEnter();
            author = askUserString("Author: ");
            author = capitalizeFirstLetterOfEachWord(author);
            List<Book> books = bookDao.getBooks("getBooksByAuthor", "%" + author + "%");
            showBooksOrNot(books);
        } catch (BookException be) {
            printEnter();
            printBlueAndEnter("Book with author: " + author + " not found");
            printEnter();
        }
    }

    /**
     * This method shows a list of books which are queried by genre
     */
    public static void showBooksByGenre() {

        Genres genre = null;
        try {
            printEnter();
            genre = askUserBookGenre("Genre: ");
            List<Book> books = bookDao.getBooks("getBooksByGenre", genre);
            showBooksOrNot(books);
        } catch (BookException be) {
            printEnter();
            printBlueAndEnter("Book with genre: " + genre + " not found");
            printEnter();
        }
    }

    /**
     * This method shows a random book
     */
    public static void showRandomBook() {

        try {
            Book book = bookDao.getRandomBook();
            if(book != null) printBook(book);
            else printBlueAndEnter("No random book found");
        } catch (BookException be) {
            printEnter();
            printBlueAndEnter("No random book found");
            printEnter();
        }
    }

    /**
     * This method shows a list of all the books ordered by title and author
     */
    public static void showAllBooksOrderedByTitleAndAuthor() {
        List<Book> books = bookDao.getBooks("getAllBooksOrderedByTitleAuthor");
        showBooksOrNot(books);
    }

    /**
     * This method shows a list of all the books ordered by title
     */
    public static void showArchivedBooksOrderedByTitle() {
        List<ArchivedBook> archivedBooks = archivedBookDao.getArchivedBooks("getArchivedBooksOrderedByTitle");
        showArchivedBooksOrNot(archivedBooks);
    }

    /**
     * This method shows a list of all the archived books ordered by the archived date
     */
    public static void showArchivedBooksOrderedByDateArchived() {
        List<ArchivedBook> archivedBooks = archivedBookDao.getArchivedBooks("getArchivedBooksOrderedByArchivedDate");
        showArchivedBooksOrNot(archivedBooks);
    }

    /**
     * This method shows a list of books
     * @param books the list of books we want to show
     */
    public static void showBooks(List<Book> books) {
        if(books == null) throw new BookException("books cannot be null");

        printBlueAndEnter("\n-------------------------------------------------------" +
                "-----------------------------------------------------------------------------");
        printBookHeading();
        books.stream().map(Object::toString).forEach(PrintTool::printBlueAndEnter);
        printBlueAndEnter("-------------------------------------------------------" +
                "-----------------------------------------------------------------------------");
        printEnter();
    }

    /**
     * This method shows a list of ordered books
     * @param comparator determines the criteria by which we order the list of books
     */
    public static void showSortedBooks(Comparator<Book> comparator) {
        if(comparator == null) throw new BookException("comparator cannot be null");

        List<Book> books = bookDao.getBooks("getAllBooks").stream().sorted(comparator).collect(Collectors.toList());
        if(books.size() > 0) {
            printBlueAndEnter("\n-------------------------------------------------------" +
                    "-----------------------------------------------------------------------------");
            printBookHeading();
            bookDao.getBooks("getAllBooks").stream()
                    .sorted(comparator).map(Book::toString).forEach(PrintTool::printBlueAndEnter);
            printBlueAndEnter("-------------------------------------------------------" +
                    "-----------------------------------------------------------------------------");
        } else{
            printEnter();
            printBlueAndEnter("No books found");
        }

        printEnter();
    }

    /**
     * This method shows a list if archived books
     * @param archivedBooks is the list of archived books we want to show
     */
    public static void showArchivedBooks(List<ArchivedBook> archivedBooks) {
        if(archivedBooks == null) throw new BookException("archivedBooks cannot be null");

        printBlueAndEnter("\n-------------------------------------------------------" +
                "-----------------------------------------------------------------------------" +
                "---------------------");
        printArchivedBookHeading();
        archivedBooks.stream().map(ArchivedBook::toString).forEach(PrintTool::printBlueAndEnter);
        printBlueAndEnter("-------------------------------------------------------" +
                "-----------------------------------------------------------------------------" +
                "---------------------");
        printEnter();
    }

    /**
     * This method determines if the list books is shown or not
     * @param books is a list of books we want to show or not
     * @throws BookException if the list of books is null
     */
    public static void showBooksOrNot(List<Book> books) {
        if(books == null) throw new BookException("books cannot be null");
        else if(books.size() > 0) showBooks(books);
        else {
            printBlueAndEnter("No books found");
            printEnter();
        }
    }

    /**
     * The method determines if the list of archived books is shown or not
     * @param archivedBooks is the list of archived books we want to show or not
     * @throws BookException if the list of archived books is null
     */
    public static void showArchivedBooksOrNot(List<ArchivedBook> archivedBooks) {
        if(archivedBooks == null) throw new BookException("archivedBooks cannot be null");
        else if(archivedBooks.size() > 0) showArchivedBooks(archivedBooks);
        else {
            printEnter();
            printBlueAndEnter("No books found");
            printEnter();
        }
    }
}