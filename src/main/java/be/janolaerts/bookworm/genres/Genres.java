package be.janolaerts.bookworm.genres;

/**
 * @author Jan
 * Enum for the genres
 */
public enum Genres {

    ADVENTURE, CONTEMPORARY, DRAMA, FANTASY, MYSTERY, ROMANCE, SCI_FI, THRILLER,
    WESTERN
}