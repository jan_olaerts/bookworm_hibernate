package be.janolaerts.bookworm.app;

import static be.janolaerts.bookworm.menus.Menu.*;
import static be.janolaerts.bookworm.tools.HibernateTool.*;
import static be.janolaerts.bookworm.tools.PrintTool.*;

/**
 * @author Jan
 * Starts the app
 */
public class App {

    /**
     * Entry point for the app
     * @param args optional arguments for the app
     * This method starts the app and closes the entity manager when the app shuts down
     */
    public static void main(String[] args) {
        App app = new App();
        app.start();
        closeEntityManagerFactory();
    }

    /**
     * This method shows the principal menu
     */
    public void start() {

        int choice;
        do {
            choice = showHeadMenu();
            printEnter();
        } while(choice != 0);
    }
}