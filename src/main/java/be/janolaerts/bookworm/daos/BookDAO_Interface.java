package be.janolaerts.bookworm.daos;

import be.janolaerts.bookworm.entities.Book;
import be.janolaerts.bookworm.exception.BookException;

import java.util.List;

/**
 * @author Jan
 * Listing of the abstract methods for the BookDao
 */
public interface BookDAO_Interface {

    void saveOrDeleteBook(Book book, String operation) throws BookException;
    Book getBook(String queryStr, Object... params) throws BookException;
    Book getRandomBook() throws BookException;
    List<Book> getBooks(String queryStr, Object... params) throws BookException;
}