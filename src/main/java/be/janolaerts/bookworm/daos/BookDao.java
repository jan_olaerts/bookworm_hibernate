package be.janolaerts.bookworm.daos;

import be.janolaerts.bookworm.entities.Book;
import be.janolaerts.bookworm.exception.BookException;
import be.janolaerts.bookworm.tools.HibernateTool;

import javax.persistence.*;
import java.util.List;
import java.util.Random;

/**
 * @author Jan
 * This class houses all the connection methods for the Book entity
 */
public class BookDao implements BookDAO_Interface {

    /**
     * This method saves or deletes a Book object to the db
     * @param book is the book to be saved
     * @param operation is save or delete
     * @throws BookException in case something goes wrong with the operation
     */
    @Override
    public void saveOrDeleteBook(Book book, String operation) throws BookException {
        if(book == null) throw new BookException("book cannot be null");
        if(!operation.equals("save") && !operation.equals("delete"))
            throw new BookException("operation must be save, update or delete");

        try {
            EntityManager em = HibernateTool.getEntityManager();
            em.getTransaction().begin();
            switch (operation) {
                case "save":
                    em.persist(book);
                    break;
                case "delete":
                    em.remove(em.contains(book)? book : em.merge(book));
                    break;
            }

            em.getTransaction().commit();

        } catch (RuntimeException re) {
            throw new BookException(re);
        } finally {
            HibernateTool.closeEntityManager();
        }
    }

    /**
     * This method fetches a book from the db
     * @param queryStr is the query by which we get a Book object
     * @param params are the optional params for the given query
     * @return a Book object
     * @throws BookException in case something goes wrong with the query
     */
    @Override
    public Book getBook(String queryStr, Object... params) throws BookException {
        if(queryStr == null) throw new BookException("queryStr cannot be null");

        Book book;

        try {
            EntityManager em = HibernateTool.getEntityManager();

            TypedQuery<Book> query = em.createNamedQuery(queryStr, Book.class);
            if(params.length != 0) setParams(query, params);
            query.setMaxResults(1);
            book = query.getSingleResult();

        } catch (RuntimeException re) {
            throw new BookException(re);
        } finally {
            HibernateTool.closeEntityManager();
        }

        return book;
    }

    /**
     * This method gets a random Book object from the db
     * @return a random Book object
     * @throws BookException in case something goes wrong with the query
     */
    @Override
    public Book getRandomBook() throws BookException {

        Book book;
        Random rand = new Random();

        try {
            EntityManager em = HibernateTool.getEntityManager();

            TypedQuery<Long> query = em.createNamedQuery("getBookCount", Long.class);
            query.setMaxResults(1);
            long count = query.getSingleResult();
            int number = rand.nextInt((int) count);
            TypedQuery<Book> selectQuery = em.createNamedQuery("getAllBooks", Book.class);
            selectQuery.setFirstResult(number);
            selectQuery.setMaxResults(1);
            book =  selectQuery.getSingleResult();

        } catch (RuntimeException re) {
            throw new BookException(re);
        } finally {
            HibernateTool.closeEntityManager();
        }

        return book;
    }

    /**
     * This method gets a list of books from the db
     * @param queryStr is the query by which we get a list of Book objects
     * @param params are the optional params for the given query
     * @return a list of Book objects
     * @throws BookException in case something goes wrong with the query
     */
    @Override
    public List<Book> getBooks(String queryStr, Object... params) throws BookException {
        if(queryStr == null) throw new BookException("queryStr cannot be null");

        List<Book> books;

        try {
            EntityManager em = HibernateTool.getEntityManager();
            em.getTransaction().begin();

            TypedQuery<Book> query = em.createNamedQuery(queryStr, Book.class);
            if(params.length > 0) setParams(query, params);
            books = query.getResultList();

            em.getTransaction().commit();
        } catch (RuntimeException re) {
            throw new BookException(re);
        } finally {
            HibernateTool.closeEntityManager();
        }

        return books;
    }

    public static void setParams(TypedQuery<Book> query, Object... params) throws BookException {
        if(query == null) throw new BookException("query cannot be null");

        for(int i = 0; i < params.length; i++) {
            if(params[i] == null) throw new BookException("Param cannot be null");
            query.setParameter(i+1, params[i]);
        }
    }
}