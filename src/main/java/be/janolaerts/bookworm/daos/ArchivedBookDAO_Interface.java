package be.janolaerts.bookworm.daos;

import be.janolaerts.bookworm.entities.ArchivedBook;
import be.janolaerts.bookworm.exception.BookException;

import java.util.List;

/**
 * @author Jan
 * Listing of abstract methods for the ArchivedBookDao
 */
public interface ArchivedBookDAO_Interface {
    void saveArchivedBook(ArchivedBook archivedBook) throws BookException;
    List<ArchivedBook> getArchivedBooks(String queryStr) throws BookException;
}