package be.janolaerts.bookworm.daos;

import be.janolaerts.bookworm.entities.ArchivedBook;
import be.janolaerts.bookworm.exception.BookException;
import be.janolaerts.bookworm.tools.HibernateTool;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * @author Jan
 * This class houses all the connection methods for the ArchivedBook entity
 */
public class ArchivedBookDAO implements ArchivedBookDAO_Interface {

    /**
     * This method daves an ArchivedBook object to the db
     * @param archivedBook is the ArchivedBook object to be saved
     */
    @Override
    public void saveArchivedBook(ArchivedBook archivedBook) {
        if(archivedBook == null) throw new BookException("archivedBook cannot be null");

        try {
            EntityManager em = HibernateTool.getEntityManager();
            em.getTransaction().begin();
            em.persist(archivedBook);
            em.getTransaction().commit();
        } catch (RuntimeException re) {
            throw new BookException(re);
        } finally {
            HibernateTool.closeEntityManager();
        }
    }

    /**
     * This method gets an ArchivedBook object based on the given query
     * @param queryStr is the query by which we get an ArchivedBook object
     * @return a list of found ArchivedBook objects
     * @throws BookException in case of a problem while executing the query
     */
    @Override
    public List<ArchivedBook> getArchivedBooks(String queryStr) throws BookException {
        if(queryStr == null) throw new BookException("queryStr cannot be null");

        List<ArchivedBook> archivedBooks = null;

        try {
            EntityManager em = HibernateTool.getEntityManager();
            em.getTransaction().begin();

            TypedQuery<ArchivedBook> query = em.createNamedQuery(queryStr, ArchivedBook.class);
            archivedBooks = query.getResultList();

            em.getTransaction().commit();
        } catch (RuntimeException re) {
            System.err.println("Exception in getBooks method: " + re.getMessage());
        } finally {
            HibernateTool.closeEntityManager();
        }

        return archivedBooks;
    }
}