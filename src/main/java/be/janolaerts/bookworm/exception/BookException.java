package be.janolaerts.bookworm.exception;

/**
 * @author Jan
 * This class houses the BookException for when something goes wrong with a Book object or an ArchivedBook object
 */
public class BookException extends RuntimeException {

    /**
     * Constructor for a BookException object
     * @param cause is most likely another exception object
     */
    public BookException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructor for a BookException object
     * @param message is the message for the BookException object
     */
    public BookException(String message) {
        super(message);
    }
}