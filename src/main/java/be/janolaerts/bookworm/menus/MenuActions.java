package be.janolaerts.bookworm.menus;

import be.janolaerts.bookworm.daos.ArchivedBookDAO;
import be.janolaerts.bookworm.daos.BookDao;
import be.janolaerts.bookworm.entities.ArchivedBook;
import be.janolaerts.bookworm.entities.Book;
import be.janolaerts.bookworm.exception.BookException;

import static be.janolaerts.bookworm.menus.Menu.*;
import static be.janolaerts.bookworm.show.ShowBooks.showBookByIsbnOrTitle;
import static be.janolaerts.bookworm.tools.PrintTool.*;
import static be.janolaerts.bookworm.tools.PrintTool.printEnter;
import static be.janolaerts.bookworm.tools.UserInputTool.askPressEnterToContinue;
import static be.janolaerts.bookworm.tools.UserInputTool.askYesOrNo;

/**
 * @author Jan
 * This class houses the methods for going to a submenu and archiving a book
 */
public class MenuActions {

    private static final BookDao bookDao = new BookDao();
    private static final ArchivedBookDAO archivedBookDao = new ArchivedBookDAO();

    /**
     * This method lets a user pick a submenu
     * @param choice is the submenu the user wants to view
     * @return the choice of the user
     * @throws IllegalArgumentException is the choice is not between 0 and 4
     */
    public static int determineSubMenu(int choice) {
        if(choice < 0 || choice > 4) throw new IllegalArgumentException(
                "choice cannot be smaller than 0 or larger than 4");

        switch(choice) {
            case 1:
                showBooksMenu();
                break;
            case 2:
                showArchivedMenu();
                break;
            case 3:
                addBookMenu();
                break;
            case 4:
                archiveBook();
                break;
            case 0:
                break;
        }

        return choice;
    }

    /**
     * This method allows the user to archive a book
     */
    public static void archiveBook() {
        Book book = showBookByIsbnOrTitle("title");
        if(book != null) {
            if(askYesOrNo("Do you want to archive this book? (y/n): ")) {
                try {
                    bookDao.saveOrDeleteBook(book, "delete");
                    archivedBookDao.saveArchivedBook(new ArchivedBook(book));
                    printEnter();
                    printYellowAndEnter("Book successfully archived");
                    printEnter();
                } catch (BookException be) {
                    printYellowAndEnter("Error while archiving book");
                    printEnter();
                }

            } else {
                printYellowAndEnter(ANSI_YELLOW + "Leaving book in the database" + ANSI_RESET);
            }
        }

        askPressEnterToContinue();
        printEnter();
    }
}