package be.janolaerts.bookworm.menus;

import be.janolaerts.bookworm.exception.BookException;
import be.janolaerts.bookworm.genres.Genres;
import be.janolaerts.bookworm.daos.BookDao;
import be.janolaerts.bookworm.entities.Book;

import static be.janolaerts.bookworm.menus.MenuActions.determineSubMenu;
import static be.janolaerts.bookworm.tools.PrintTool.*;
import static be.janolaerts.bookworm.tools.StringTool.capitalizeFirstLetterOfEachWord;
import static be.janolaerts.bookworm.tools.UserInputTool.*;
import static be.janolaerts.bookworm.show.ShowBooks.*;

/**
 * @author Jan
 * This class houses the different menus in the app
 */
public class Menu {

    private static final BookDao bookDao = new BookDao();

    /**
     * This method shows the principal menu
     * @return the choice the user put in
     */
    public static int showHeadMenu() {
        printBlueAndEnter("Welcome To The Bookworm App!");
        printBlueAndEnter("--------------------------------------");
        printBlueAndEnter("1| Show Books Menu");
        printBlueAndEnter("2| Show Archived Books Menu");
        printBlueAndEnter("3| Add A Book");
        printBlueAndEnter("4| Archive Book");
        printBlueAndEnter(ANSI_YELLOW + "0| Exit" + ANSI_RESET);
        printBlueAndEnter("--------------------------------------");

        return determineSubMenu(askUserIntBetweenRange(
                "Give a number between 0 and 4: ", 0, 4));
    }

    /**
     * This method shows the menu for viewing the books
     */
    public static void showBooksMenu() {

        int choice;
        do{
            printEnter();
            printBlueAndEnter("Books Menu");
            printBlueAndEnter("-------------------------------------------------------------------------------------");
            printBlueAndEnter(" 1 | Show All Books Ordered By Id                   7 | Get Book By Isbn");
            printBlueAndEnter(" 2 | Show All Books Ordered By Title                8 | Get Book By Title");
            printBlueAndEnter(" 3 | Show All Books Ordered By Author               9 | Get Books By Author");
            printBlueAndEnter(" 4 | Show All Books Ordered By Isbn                10 | Get Books By Genre");
            printBlueAndEnter(" 5 | Show All Books Ordered By Genre               11 | Get A Random Book");
            printBlueAndEnter(" 6 | Show All Books Ordered By Title and Author    12 | Get First Book");
            printBlueAndEnter("13 | Get Last Book                                 " +
                    ANSI_YELLOW + " 0 | Back To The Main Menu " + ANSI_RESET);
            printBlueAndEnter("-------------------------------------------------------------------------------------");
            choice = doShowBooks(askUserIntBetweenRange(
                    "Give a number between 0 and 13: ", 0, 13));

        } while(choice != 0);
    }

    /**
     * This method shows the menu for viewing the archived books
     */
    public static void showArchivedMenu() {

        int choice;
        do {
            printEnter();
            printBlueAndEnter("Archived Books Menu");
            printBlueAndEnter("--------------------------------------------------------");
            printBlueAndEnter("1 | Show All Archived Books Ordered By Title");
            printBlueAndEnter("2 | Show All Archived Books Ordered By Date Archived");
            printYellowAndEnter("0 | Head menu");
            printBlueAndEnter("--------------------------------------------------------");
            choice = doShowArchivedBooks(askUserIntBetweenRange(
                    "Give a number between 0 and 2: ", 0, 2));
        } while(choice != 0);
    }

    /**
     * This method allows a user to add a book
     */
    public static void addBookMenu() {

        try {
            printEnter();
            printBlueAndEnter("Add a Book");
            printBlueAndEnter("--------------------------------------------");

            // Asking information to add a book
            String title = askUserString("Title: ");
            title = capitalizeFirstLetterOfEachWord(title);

            String author = askUserString("Author: ");
            author = capitalizeFirstLetterOfEachWord(author);

            String isbn = askIsbnAndCheckIfInDb();
            int revisionNr = askUserIntWithLowBoundary("Revision Nr: ", 0);
            printEnter();

            Genres genre = askUserBookGenre("Genre: ");
            boolean fiction = askYesOrNo("Fiction (y/n): ");

            Book book = new Book(title, author, isbn, revisionNr, genre, fiction);

            bookDao.saveOrDeleteBook(book, "save");
            printEnter();
            printYellowAndEnter("Book successfully saved");
            askPressEnterToContinue();
        } catch (BookException be) {
            printYellowAndEnter("Error while saving book");
            be.printStackTrace();
        }
    }
}